# -*- coding: utf-8 -*-

import mayavi.mlab as ml
import os
import numpy as np
import scipy as sp

class Waveguide_mesh():
    """Plots and optionally animates/saves the waveguide surfaces.
    Supported profiles are 'circular', 'ellipse_bulge' and 'star'."""
    def __init__(self):
        self.set_params(eps=0.2, h1=1.5, x=sp.linspace(-15,15,100), y=sp.linspace(-1, 1, 100))

    def set_params(self, eps=None, h1=None, y=None, x=None):
        if eps is not None:
            self.eps = eps
        if h1 is not None:
            self.h1 = h1
        if y is not None:
            self.y = y
        if x is not None:
            self.x = x

        self.xx, self.yy = sp.meshgrid(self.x, self.y)
        if hasattr(self,'zz'): del self.zz

    def get_params(self):
        print 'eps:         ' + str(self.eps)
        print 'h1:          ' + str(self.h1)
        print 'x (lnspc):  [' + str(self.x[0]) + ', ' + str(self.x[-1]) + ', ' + str(self.x.size) + ']' 
        print 'y (lnspc):   [' + str(self.y[0]) + ', ' + str(self.y[-1]) + ', ' + str(self.y.size) + ']'

    def h(self, x):
        o = np.ones_like(x)
        h = 1. + np.exp(-1. / (1. - x**2.))
        return np.where(abs(x) < 1., h, o)

    def set_profile(self):
        xe = self.xx * self.eps
        self.zzside = self.yy * self.h(xe)
        self.zztop = self.h(xe)

    def waveguide_mesh(self,save=False):
        if not hasattr(self,'zz'):
            self.set_profile()

        xx = self.xx
        yy = self.yy
        zzside = self.zzside
        zztop = self.zztop

        f = ml.figure(bgcolor=(1, 1, 1), size=(1280, 720))

        m1 = ml.mesh(xx,yy,zztop,figure=f,colormap="summer")
        m2 = ml.mesh(xx,yy,-zztop,figure=f,colormap="summer")

        m3 = ml.mesh(xx,sp.ones_like(yy),zzside,figure=f,colormap="summer")
        m4 = ml.mesh(xx,-sp.ones_like(yy),zzside,figure=f,colormap="summer")
        
        lut = m1.module_manager.scalar_lut_manager.lut.table.to_array()
        for ch in m3,m4:
            ch.module_manager.scalar_lut_manager.lut.table = lut[40:180,:]
        m1.module_manager.scalar_lut_manager.lut.table = lut[180:,:]
        m2.module_manager.scalar_lut_manager.lut.table = lut[40:100,:]
        ml.draw()

        ml.view(azimuth=30,elevation=80,distance=25,focalpoint=[6,0,0])
        #ml.view(azimuth=50, elevation=80, focalpoint=[1.5, 0, 0])
        ml.roll(roll=-90)
        if save == True:
            ml.savefig('rectangular.png',size=(1280,720), figure=f)
