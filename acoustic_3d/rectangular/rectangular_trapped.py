# -*- coding: utf-8 -*-
"""
Created on Wed Apr 18 12:56:14 2012

@author: Simon Gaulter
"""
import numpy as np
import scipy as sp
import scipy.special as spc
import scipy.integrate as spi
import scipy.optimize as spo
import mayavi.mlab as ml
import pylab as pl

class Rect_trapped:
	"""Provides methods to calculate the eigenvalues of the rectangular waveguide.
	set_params  -- method to set the required parameters;
	get_params  -- return current parameters;
	k_root      -- return wavenumbers, k_p, for one or more Airy or Airy' roots;
	k_eps       -- iterate and call 'k_root' over an array of epsilon values;
	k_h1        -- iterate and call 'k_root' over an array of h1 values;
	plot_result -- plot the data produced."""

	SYMMETRIC = 'symmetric'
	ANTISYMMETRIC = 'antisymmetric'
	SOUNDSOFT = 'soundsoft'
	SOUNDHARD = 'soundhard'

	def __init__(self):
		""" Initialise the class and set parameters.
		Defaults to finding a single k."""
		self.set_params(eps=0.1,h1=1.5,m=1,n=1,p=1,symm=self.SYMMETRIC,\
				multiple_p=False,bc=self.SOUNDSOFT)

	def get_params(self):
		"""Return the current parameters."""
		print "eps:        " + str(self.eps)
		print "h1:         " + str(self.h1)
		print "m:          " + str(self.m)
		print "n:          " + str(self.n)
		print "p:          " + str(self.p)
		print "symm:       " + str(self.symm)
		print "bc:         " + str(self.bc)
		print "multiple_p: " + str(self.multiple_p)

	def set_params(self,eps=None,h1=None,m=None,n=None,p=None,symm=None,\
			multiple_p=None,bc=None):
		"""Optionally change the parameters.

		Keyword arguments:
			eps        -- one or more values of epsilon to use;
			h1         -- one or more values of h1 to use;
			NOTE: Only one of the above may have dimension > 0 at a time
			m          -- transverse (y) mode;
			n          -- transverse (z) mode;
			p          -- longitudinal mode;
			symm       -- symmetric (True) or antisymmetric (False);
			multiple_p -- use multiple (True) or a single (False) Airy root."""
		if ((isinstance(eps,sp.ndarray) and eps.size > 1) \
				and (isinstance(h1,sp.ndarray) and h1.size > 1)):
			print "Error: We can iterate over a range of EITHER h1 or epsilon."
			return
		if m is not None:
			# mth transverse (y) mode
			self.m = m
		if n is not None:
			# nth transverse (z) mode
			self.n = n
		if p is not None:
			# pth longitudinal mode
			self.p = p
		if symm is not None:
			# Either symmetric or antisymmetric modes
			self.symm = symm
		if bc is not None:
			self.bc = bc
		if h1 is not None:
			# h1 max bulge height
			if isinstance(h1,sp.ndarray):
				self.h1 = h1
			else:
				self.h1 = sp.array([h1])
		if eps is not None:
			# 0 < eps << 1
			if isinstance(eps,sp.ndarray):
				self.eps = eps
			else:
				self.eps = sp.array([eps])

		# Get the pth root of either Ai (antisymmetric) or Ai' (symmetric)
		if self.symm == self.SYMMETRIC:
			self.aiz = spc.ai_zeros(self.p)[1]
		else:
			self.aiz = spc.ai_zeros(self.p)[0]
		if multiple_p is not None:
			self.multiple_p = multiple_p
		if self.multiple_p is False:
			self.aiz = sp.array([self.aiz[-1]])

		# Refresh all output once parameters are changed.
		if hasattr(self,'k'): del self.k
		if hasattr(self,'k_result_h1'): del self.k_result_h1
		if hasattr(self,'k_result_eps'): del self.k_result_eps
		if hasattr(self,'phi'): del self.phi
		if hasattr(self,'lastcalled'): del self.lastcalled

	def plot_k_result(self,kind=None,title=True,show=True):
		"""kind can be 'eps' or 'h1', to specifiy result type.
		If none is specified, use the last created k_result.
		title is a boolean which determines if the plot title is printed."""

		if kind == 'h1':
			pl.plot(self.h1,self.k_result_h1)
			xl = '$h_1$'
			t = '$\epsilon = ' + str(self.eps[0]) + '$'
		elif kind == 'eps':
			pl.plot(self.eps,self.k_result_eps)
			xl = '$\epsilon$'
			t = '$h_1 = ' + str(self.h1[0]) + '$'
		else:
			if hasattr(self,'lastcalled') and self.lastcalled == 'k_eps':
				pl.plot(self.eps,self.k_result_eps)
				xl = '$\epsilon$'
				t = '$h_1 = ' + str(self.h1[0]) + '$'
			elif hasattr(self,'lastcalled') and self.lastcalled == 'k_h1':
				pl.plot(self.h1,self.k_result_h1)
				xl = '$h_1$'
				t = '$\epsilon = ' + str(self.eps[0]) + '$'
			else:
				print "No data to plot. Call method 'k_eps' or 'k_h1' first."
				return

		pl.rcParams['mathtext.default'] = 'regular'
		if title == True:
			pl.title('Wavenumbers $k$ against ' + xl + ' for $m = '\
					+ str(self.m) + '$, $n = ' + str(self.n) + '$ and ' + t + '.')
			pl.xlabel(xl)
		pl.ylabel('$k$')
		if show == True:
			pl.show()

	def k_eps(self):
		"""Produces an ndarray of values, 'k_result_eps', for varying epsilon and fixed h1.
		May use one or more Airy, or Airy' roots."""
		if self.multiple_p == True:
			self.k_result_eps = sp.zeros([self.eps.size,self.p])
		else:
			self.k_result_eps = sp.zeros([self.eps.size,1])
		self.__eps = self.eps
		self.__aiz = self.aiz
		for i,self.eps in enumerate(self.__eps):
			for j,self.aiz in enumerate(self.__aiz):
				self.k_result_eps[i,j] = self.k_root()
		self.eps = self.__eps
		self.aiz = self.__aiz
		self.lastcalled = 'k_eps'
		#self.k_result_eps = self.k_result_eps[~sp.isnan(self.k_result_eps).all(1)]
		return self.k_result_eps

	def k_h1(self):
		"""Produces a matrix of values, 'k_result_h1', for varying h1 and fixed epsilon.
		May use one or more Airy, or Airy' roots."""
		if self.multiple_p == True:
			self.k_result_h1 = sp.zeros([self.h1.size,self.p])
		else:
			self.k_result_h1 = sp.zeros([self.h1.size,1])
		self.__h1 = self.h1
		self.__aiz = self.aiz
		for i,self.h1 in enumerate(self.__h1):
			for j,self.aiz in enumerate(self.__aiz):
				self.k_result_h1[i,j] = self.k_root()
		self.h1 = self.__h1
		self.aiz = self.__aiz
		self.lastcalled = 'k_h1'
		#self.k_result_h1 = self.k_result_h1[~sp.isnan(self.k_result_h1).all(1)]
		return self.k_result_h1

	def k_root(self,m=None,n=None,p=None):
		"""Find a single wavenumber for one h1, epsilon and Airy or Airy' root.
		Use methods 'k_h1' or 'k_eps' to produce data for a range of h1 or eps, respectively."""
		if m is not None:
			self.set_params(m=m)
		if n is not None:
			self.set_params(n=n)
		if p is not None:
			self.set_params(p=p)
		I = lambda k: -self.g(0.,k) + self.eps**(2./3)*self.aiz
		# Find the root - Brent algorithm
		try:
			self.k = spo.brentq(I,self.tau(0) + 1.E-12,self.tau(2) - 1.E-12, maxiter=1000)
		except:
			self.k = sp.nan
		return self.k

	def g(self,x,k):
		"""Return the phase function, g(x,k). Takes only positive values of x."""
		it = sp.nditer([x,None])
		for xx,y in it:
			if xx < self.xstar(k):
				y[...] = -(3./2 * spi.quad(lambda x: sp.sqrt(k**2 - (self.tau(x))**2),\
						xx, self.xstar(k))[0])**(2./3)
			else:
				y[...] = (3./2 * spi.quad(lambda x: sp.sqrt((self.tau(x))**2 - k**2),\
						self.xstar(k), xx)[0])**(2./3)
		return it.operands[1]

	def xstar(self,k):
		"""Return the turning point, x_star."""
		xs = spo.brentq(lambda x: k**2. - self.tau(x)**2.,0.,1 - 1e-12)
		#xs = np.sqrt( 1. + 1. / (np.log( self.n * np.pi \
				#/ ( 2. * np.sqrt(abs(k**2. - (self.m * np.pi / 2.)**2.) )) - 1 )))
		return xs if xs < 1 else None

	def h(self, x):
		o = np.ones_like(x)
		h = 1. + np.exp(-1. / (1. - x**2.))
		return np.where(abs(x) < 1., h, o)

	def tau(self,x):
		"""Return the transverse eigenvalue, tau_mn(x)."""
		return sp.sqrt( ( self.m * np.pi / 2. )**2. \
				+ ( self.n * np.pi / ( 2. * self.h(x) ) )**2. )


class Rect_mesh(Rect_trapped):
	"""Subclass to calculate phi(x,r,0) and plot the solution for the circular waveguide."""

	def __init__(self):
		"""Initialise the (sub)class. y will always be zero."""
		Rect_trapped.__init__(self)
		self.set_plot_params(x=[0,15,50],z=[-1,1,50])
		#self.set_params(m=2)
		self.y = -0.5

	def set_plot_params(self,x=None,y=None,z=None):
		"""Set up the linspaces for x and z. The actual region
		will be reflected across zero for x."""
		if x is not None:
			self.x = sp.linspace(x[0],x[1],x[2])
		if z is not None:
			self.z = sp.linspace(z[0],z[1],z[2])
		if y is not None:
			self.y = y
		if hasattr(self,'phi'): del self.phi

	def get_params(self):
		"""Return the currently defined linspaces."""
		print "x:          [" + str(self.x[0]) + ", " + str(self.x[-1]) + ", " + str(self.x.size) + "]"
		print "z:          [" + str(self.z[0]) + ", " + str(self.z[-1]) + ", " + str(self.z.size) + "]"

		Rect_trapped.get_params(self)

	def phi_calc(self):
		"""Calculate the values of phi(x,r,0).
		This will give us a 'cross section' of the data at theta = {0,2*pi}."""
		# Calculate k for the current values if it doesn't already exist.
		self.k_root()

		# Set up the meshes.
		self.__xx,self.__zz = sp.meshgrid(self.x,self.z)
		self.__xe = self.eps*self.__xx
		self.__zz = self.__zz*self.h(self.__xe)

		# Return phi(x,z,0)
		if self.soundhard == True:
			phi =(sp.absolute(self.g(self.__xe,self.k))**(.25)\
					* sp.cos(self.m * sp.pi * (self.y + 1.) / 2.)\
					* sp.cos(self.n * sp.pi * (self.__zz + self.h(self.__xe)) / (2. * self.h(self.__xe)))\
					* spc.airy(self.eps**(-2./3) * self.g(self.__xe,self.k))[0])\
					/ sp.absolute(self.k**2 - self.tau(self.__xe)**2)**(.25)
		else:
			phi =(sp.absolute(self.g(self.__xe,self.k))**(.25)\
					* sp.sin(self.m * sp.pi * (self.y + 1.) / 2.)\
					* sp.sin(self.n * sp.pi * (self.__zz + self.h(self.__xe)) / (2. * self.h(self.__xe)))\
					* spc.airy(self.eps**(-2./3) * self.g(self.__xe,self.k))[0])\
					/ sp.absolute(self.k**2 - self.tau(self.__xe)**2)**(.25)

			#phi =(sp.absolute(self.g(self.__xe,self.k))**(.25)\
					#* sp.cos(self.m * self.th) * spc.jn(self.m,self.__rr\
					#* self.tau(self.__xe)) * spc.airy( self.eps**(-2./3)\
					#* self.g(self.__xe,self.k) )[0]) / (self.h(self.__xe)\
					#* sp.absolute(self.k**2 - self.tau(self.__xe)**2)**(.25))
		#else:
			#phi =(sp.absolute(self.g(self.__xe,self.k))**(.25)\
					#* sp.cos(self.m * self.th) * spc.jn(self.m,self.__rr\
					#* self.tau(self.__xe)) * spc.airy( self.eps**(-2./3)\
					#* self.g(self.__xe,self.k) )[0]) / (self.h(self.__xe)\
					#* sp.absolute(self.k**2 - self.tau(self.__xe)**2)**(.25))

		# Reflect phi around the x = 0 plane, taking symmetry
		# or antisymmetry into account.
		if self.symm == True:
			phi = sp.hstack([phi[:,-1:0:-1],phi])
		else:
			phi = sp.hstack([-phi[:,-1:0:-1],phi])

		# Reflect phi 'around' the azimuth, since cos(0) = -cos(2*pi).
		#self.phi = sp.vstack([-phi[-1:0:-1],phi])
		self.phi = phi
		# Reflect x and r as well, to give a suitable mesh grid.
		x = sp.hstack([-self.__xx[:,-1:0:-1],self.__xx])
		#self.__x_plot = sp.vstack([x[-1:0:-1],x])
		self.__x_plot = x
		z = sp.hstack([self.__zz[:,-1:0:-1],self.__zz])
		#self.__r_plot = sp.vstack([-r[-1:0:-1],r])
		self.__z_plot = z
		return self.phi

	def plot_phi_mesh(self,**kwargs):
		"""Plot phi(x,r,0), calculating it first if necessary."""
		if not hasattr(self,'phi'):
			self.phi_calc()
		if not hasattr(self,'fig'): self.fig = ml.figure(bgcolor=(1,1,1),size=(1600,1400))
		lim = max(abs(self.phi.max()),abs(self.phi.min()))
		ml.mesh(self.__x_plot,self.__z_plot,self.phi,figure=self.fig,vmin=-lim,vmax=lim,**kwargs)

		ml.view(azimuth=164,elevation=62,distance=30,focalpoint=[-0.657,-1.36,-1.534])
		ml.roll(4.3)
		#ml.pitch(-5.5)
		#self.scene = ml.get_engine().scenes[0]

	def plot_waveguide_mesh(self,**kwargs):
		"""Plot the waveguide shell, with transparency."""
		y = sp.linspace(-1,1,100)
		xx,yy = sp.meshgrid(sp.hstack([-self.x[-1:0:-1],self.x]),y)

		xe = xx * self.eps
		zztop = yy * self.h(xe)
		zzside = self.h(xe)

		if not hasattr(self,'fig'): self.fig = ml.figure(bgcolor=(1,1,1),size=(1600,1400))
		ml.mesh(xx,zzside,yy,figure=self.fig,colormap="summer",opacity=0.2,**kwargs)
		ml.mesh(xx,-zzside,yy,figure=self.fig,colormap="summer",opacity=0.2,**kwargs)
		ml.mesh(xx,zztop,sp.ones_like(yy),figure=self.fig,colormap="summer",opacity=0.2,**kwargs)
		ml.mesh(xx,zztop,-sp.ones_like(yy),figure=self.fig,colormap="summer",opacity=0.2,**kwargs)
		ml.view(azimuth=164,elevation=62,distance=30,focalpoint=[-0.657,-1.36,-1.534])
		ml.roll(4.3)
		#ml.pitch(-5.5)
		#self.scene = ml.get_engine().scenes[0]
		#self.scene.scene.background=(.9,.9,.9)
