from rectangular_trapped import Rect_mesh
import mayavi.mlab as m

r = Rect_mesh()

n = [1, 2]
p = [1, 2]

r.set_params(soundhard=True)

for ni in n:
	for pi in p:
		print "Plotting n = {0}, p = {1} (antisymmetric)...".format(*[ni,pi])
		r.set_params(n=ni,p=pi,symm=False)
		m.clf()
		r.plot_waveguide_mesh()
		r.plot_phi_mesh()
		m.savefig('solution_plots/neumann/asymm_n{0}p{1}.png'.format(*[ni,pi]), size=[1280,720])

		print "Plotting n = {0}, p = {1} (symmetric)...".format(*[ni,pi])
		r.set_params(n=ni,p=pi,symm=True)
		m.clf()
		r.plot_waveguide_mesh()
		r.plot_phi_mesh()
		m.savefig('solution_plots/neumann/symm_n{0}p{1}.png'.format(*[ni,pi]), size=[1280,720])
