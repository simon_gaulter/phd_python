from rectangular_spectral import Rect_spectral
import numpy as np
import datetime
import time

r = Rect_spectral()

# X 6-42
X = [ n * 2 for n in range(35,39) ]
Y = [ n * 2 for n in range(3,7) ]
Z = [ n * 2 for n in range(3,6) ]

nvals = 40
rold = np.load('res2_x6-68_y6-12_z6-10.npy')
nold = rold.shape[1]
res = np.zeros([nvals, nold + len(X), len(Y), len(Z)])
res[:,0:nold,:,:] = rold

for i in range(0,len(X)):
	 for j in range(0,len(Y)):
		 for k in range(0,len(Z)):
			print "X={0}, Y={1}, Z={2}".format(X[i],Y[j],Z[k])
			print "Started at {0}".format(time.ctime())
			tstart = time.time()
			r.set_params(X=X[i],Y=Y[j],Z=Z[k])
			r.k_h1()
			res[:,i + nold,j,k] = r.k_result_h1[0][0:nvals]
			np.save('res',res)
			print "{0} elapsed".format(datetime.timedelta(seconds=round(time.time()-tstart)))
			print "\n" + "-"*20 + "\n"

