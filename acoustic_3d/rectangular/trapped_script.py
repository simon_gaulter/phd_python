from rectangular_trapped import Rect_trapped
r = Rect_trapped()
import numpy as np

res = np.zeros([4,4,4])
for i in range(1,5):
	for j in range(1,5):
		for k in range(1,5):
			r.set_params(m=i,n=j,p=k)
			res[i-1,j-1,k-1] = r.k_root()

print res
