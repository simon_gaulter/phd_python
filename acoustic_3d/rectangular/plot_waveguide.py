import mayavi.mlab as ml
import scipy as sp


def h(x):
	o = sp.ones_like(x)
	h = 1. + sp.exp(-1. / (1. - x**2.))
	return sp.where(abs(x) < 1., h, o)

eps = 0.1
x = sp.linspace(-15,15,100)
y = sp.linspace(-1,1,100)
xx,yy = sp.meshgrid(x,y)



xe = xx * eps
zztop = yy * h(xe)
zzside = h(xe)

ml.figure(bgcolor=(1,1,1,),size=(1280,720))
ms1 = ml.mesh(xx,zzside,yy,colormap="summer")
ms2 = ml.mesh(xx,-zzside,yy,colormap="summer")
mt1 = ml.mesh(xx,zztop,sp.ones_like(yy),colormap="summer")
mt2 = ml.mesh(xx,zztop,-sp.ones_like(yy),colormap="summer")


lut = ms1.module_manager.scalar_lut_manager.lut.table.to_array()
		#ch.module_manager.scalar_lut_manager.lut.table = lut[40:180,:]
ms1.module_manager.scalar_lut_manager.lut.table = lut[200:,:]
ms2.module_manager.scalar_lut_manager.lut.table = lut[40:80,:]
mt1.module_manager.scalar_lut_manager.lut.table = lut[180:,:]
mt2.module_manager.scalar_lut_manager.lut.table = lut[40:100,:]

ml.view(azimuth=164,elevation=62,distance=30,focalpoint=[-0.657,-1.36,-1.534])
ml.roll(4.3)

