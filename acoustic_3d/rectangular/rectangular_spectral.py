import numpy as np
import scipy as sp
import scipy.special as spc
import scipy.linalg as spl
import scipy.io as spio
import pylab as pl

class Rect_spectral:
    # TODO - docstring

    SYMMETRIC = 'symmetric'
    ANTISYMMETRIC = 'antisymmetric'
    SOUNDSOFT = 'soundsoft'
    SOUNDHARD = 'soundhard'

    def __init__(self):
        self.set_params(h1=1.5,eps=0.1,m=1,n=1,p=1,\
                symm=self.SYMMETRIC,bc=self.SOUNDSOFT,X=10,Y=10,Z=10)

    def set_params(self,h1=None,eps=None,m=None,n=None,p=None,\
            symm=None,bc=None,X=None,Y=None,Z=None):
        if h1 is not None:
            if isinstance(h1,np.ndarray):
                self.h1 = h1
            else:
                self.h1 = np.array([h1])
        if eps is not None:
            if isinstance(eps,np.ndarray):
                self.eps = eps
            else:
                self.eps = np.array([eps])
        if m is not None:
            self.m = m
        if n is not None:
            self.n = n
        if p is not None:
            self.p = p
        if symm is not None:
            self.symm = symm
        if bc is not None:
            self.bc = bc
        if X is not None:
            self.X = X
        if Y is not None:
            self.Y = Y
        if Z is not None:
            self.Z = Z

    def get_params(self):
        """Return the current parameters."""
        print "h1:          " + str(self.h1)
        print "eps:         " + str(self.eps)
        print "m:           " + str(self.m)
        print "n:           " + str(self.n)
        print "p:           " + str(self.p)
        print "symm:        " + str(self.symm)
        print "bc:          " + str(self.bc)
        print "X-by-Y-by-Z: " + str(self.X) + "-by-" + str(self.Y) + "-by-" + str(self.Z)

    def plot_k_result(self, kind=None, title=True, show=True):
        """kind can be 'eps' or 'h1', to specifiy result type.
        If none is specified, use the last created k_result.
        title is a boolean which determines if the plot title is printed."""

        if kind == 'h1':
            pl.plot(self.h1,self.k_result_h1,'x')
            xl = '$h_1$'
            t = '$\epsilon = ' + str(self.eps[0]) + '$'
        elif kind == 'eps':
            pl.plot(self.eps,self.k_result_eps,'x')
            xl = '$\epsilon$'
            t = '$h_1 = ' + str(self.h1[0]) + '$'
        else:
            if hasattr(self,'lastcalled') and self.lastcalled == 'k_eps':
                pl.plot(self.eps,self.k_result_eps,'x')
                xl = '$\epsilon$'
                t = '$h_1 = ' + str(self.h1[0]) + '$'
            elif hasattr(self,'lastcalled') and self.lastcalled == 'k_h1':
                pl.plot(self.h1,self.k_result_h1,'x')
                xl = '$h_1$'
                t = '$\epsilon = ' + str(self.eps[0]) + '$'
            else:
                print "No data to plot. Call method 'k_eps' or 'k_h1' first."
                return

        pl.rcParams['mathtext.default'] = 'regular'
        if title == True:
            pl.title('Wavenumbers $k$ against ' + xl + ' for $m = '\
                    + str(self.m) + '$, $n = ' + str(self.n) + '$, $p = '\
                    + str(self.p) + '$ and ' + t + '.')
            pl.xlabel(xl)
        pl.ylabel('$k$')
        if show == True:
            pl.show()

    def k_h1(self):
        if self.bc == self.SOUNDSOFT:
            eig = np.empty([self.h1.size,(self.X-1)*(self.Y-2)*(self.Z-2)])
        else:
            eig = np.empty([self.h1.size,(self.X-1)*(self.Y)*(self.Z)])
        for i,h in enumerate(self.h1):
            eig[i,:] = self.spectral(h1=h)
        #TODO - rejig this
        #eig[eig>spc.jn_zeros(self.m,self.n)[-1]] = sp.nan
        eig = eig[:,np.any(~np.isnan(eig),axis=0)]
        self.k_result_h1 = eig
        self.lastcalled = 'k_h1'

    def k_eps(self):
        eig = np.empty([self.eps.size,(self.X-1)*(self.Y-2)*(self.Z-2)])
        for i,e in enumerate(self.eps):
            eig[i,:] = self.spectral(eps=e)
        #TODO - rejig this
        #eig[eig>spc.jn_zeros(self.m,self.n)[-1]] = sp.nan
        eig = eig[:,np.any(~np.isnan(eig),axis=0)]
        self.k_result_eps = eig
        self.lastcalled = 'k_eps'

    def spectral(self,h1=None,eps=None):
        """Calculate the Helmholtz differentiation matrix
        and return the eigenvalues."""
        Z = self.Z
        Y = self.Y
        X = self.X
        if h1 is None:
            h1 = self.h1
        if eps is None:
            eps = self.eps

        # Chebyshev matrix in the y-direction.
        E,y = self.cheb_mat(Y)
        if self.bc == self.SOUNDSOFT:
            E2 = np.dot(E,E)[1:-1,1:-1]
            E1 = E[1:-1,1:-1]
            y = y[1:-1]
        else:
            y = spio.loadmat('cheb_n12')['y'][:,0]
            E1 = spio.loadmat('cheb_n12')['E1'].T
            E2 = spio.loadmat('cheb_n12')['E2'].T

        # Chebyshev matrix in the z-direction.
        F,z = self.cheb_mat(Z)
        if self.bc == self.SOUNDSOFT:
            F2 = np.dot(F,F)[1:-1,1:-1]
            F1 = F[1:-1,1:-1]
            z = z[1:-1]
        else:
            z = spio.loadmat('cheb_n12')['y'][:,0]
            F1 = spio.loadmat('cheb_n12')['E1'].T
            F2 = spio.loadmat('cheb_n12')['E2'].T

        # Laguerre matrix in the x-direction.
        if self.symm == self.ANTISYMMETRIC:
            # Dirichlet boundary for antisymmetric modes.
            D,x = self.lag_mat(X,b=X)
            D2 = np.dot(D,D)[1:,1:]
            D1 = D[1:,1:]
            x = x[1:]
        else:
            # Neumann boundary for symmetric modes.
            D,x = self.lag_mat(X,b=X)
            D2 = np.dot(D,D)
            D11 = D[0,0]
            DR1 = D[0,1:]
            DC1 = D[1:,0][np.newaxis].T
            DC2 = D2[1:,0][np.newaxis].T

            D2 = D2[1:,1:]
            D = D[1:,1:]
            x = x[1:]
            D1 = D - (1./D11) * DC1*DR1
            D2 = D2 - (1./D11) * DC2*DR1

        xx,yy,zz = np.meshgrid(x,y,z)
        xx = xx.flatten(1)
        yy = yy.flatten(1)
        zz = zz.flatten(1)

        Ix = np.eye(x.size)
        Iy = np.eye(y.size)
        Iz = np.eye(z.size)

        #################################
        #DXX = np.kron(D2, np.kron(Iy,Iz))
        #DYY = np.kron(Ix, np.kron(E2,Iz))
        #DZ = np.kron(Ix, np.kron(Iy,F1))
        #DZZ = np.kron(Ix, np.kron(Iy,F2))
        #DXZ = np.kron(D1, np.kron(Iy,F1))
        #################################
        DXX = np.kron(Iz, np.kron(D2,Iy))
        DYY = np.kron(Iz, np.kron(Ix,E2))
        DZ = np.kron(F1, np.kron(Ix,Iy))
        DZZ = np.kron(F2, np.kron(Ix,Iy))
        DXZ = np.kron(F1, np.kron(D1,Iy))


        xe = self.eps * xx
        h = self.h(xx)
        dh = self.dh(xx)
        ddh = self.ddh(xx)

        zetax = -eps * zz * dh / h
        zetay = 1. / h
        zetaxx = eps**2. * zz * (2. * dh**2. - h * ddh) / h**.2
        HH = - eps**2. * DXX \
                - 2. * eps * np.dot(np.diag(zetax), DXZ) \
                - np.dot(np.diag(zetaxx), DZ) \
                - np.dot(np.diag(zetax**2.), DZZ) \
                - np.dot(np.diag(zetay**2.), DZZ) \
                - DYY

        print "{0} - matrix calculated...".format(HH.shape)
        return np.sqrt(np.sort(np.real(spl.eig(HH)[0])))

    def h(self,x):
        o = np.ones_like(x)
        h = 1. + np.exp(-1. / (1. - x**2.))
        return np.where(abs(x) < 1., h, o)

    def dh(self, x):
        z = np.zeros_like(x)
        h = - 2. * x * np.exp(-1. / (1. - x**2.)) / ( 1. - x**2.)**2.
        return np.where(abs(x) < 1., h, z)

    def ddh(self, x):
        z = np.zeros_like(x)
        h = (6. * x**4. - 2.) * np.exp(-1. / (1. - x**2.)) / (1. - x**2.)**4.
        return np.where(abs(x) < 1., h, z)

    def cheb_mat(self,gridsize):
        if gridsize == 0:
            self.D = 0
            self.r = 1
            return
        else:
            N = gridsize - 1

        x = sp.cos(sp.pi*(np.arange(0,N+1)/float(N))).T

        c = np.vstack([[2],np.ones([N-1,1]),[2]]).conj().T*(-1.)**np.arange(0,N+1)

        X = np.tile(x,(N+1,1)).T
        dX = X - X.T
        D = (c*((1./c).T)/(dX+(np.eye(N+1))))
        D = D - np.diag(np.sum(D,axis=0))
        return -D.T, x

    def lag_mat(self,gridsize,b=1):
        M = gridsize
        x = self.__lag_roots(M-1)
        x = np.insert(x,0,0.)
        alpha = sp.exp(-x/2.)

        beta = (-.5)*np.ones(x.size)

        self.lag_D = b*self.__poldif(x,alpha,beta)

        self.lag_x = x/b

        return self.lag_D,self.lag_x

    def __lag_roots(self,M):
        J = np.diag(np.arange(1,2*M,2)) - np.diag(np.arange(1,M),1)\
                - np.diag(np.arange(1,M),-1)
        return np.sort(np.real(spl.eig(J)[0]))

    def __poldif(self,x,malpha,B):
        N = x.size

        x = x[np.newaxis].T

        alpha = malpha.reshape([malpha.size,1])

        L = np.eye(N,dtype=bool)

        DX = x - x.T
        DX[L] = 1

        c = alpha*np.prod(DX,axis=1).reshape([N,1])
        C = c/c.T

        Z = 1./DX
        Z[L] = 0

        X = Z[~L].reshape(N-1,N,order='F')

        Y = np.ones([N-1,N])
        D = np.eye(N)

        Y = np.cumsum(np.vstack([B, Y[0:N-1,:]*X]),axis=0)
        D = Z*(C*np.tile(np.diag(D),(N,1)) - D)
        D[L] = Y[N-1,:]
        return D
