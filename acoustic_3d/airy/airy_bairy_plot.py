import scipy as sp
import scipy.special as spc
import matplotlib as mpl
import matplotlib.pyplot as plt

x = sp.linspace(-15, 5, 1000)

mpl.rc('text', usetex='true')
mpl.rc('font', family='serif')
mpl.rc('font', serif=[])

ax = plt.axes()

line_ai = ax.plot(x, spc.airy(x)[0], label='$\mathrm{Ai}(x)$')
line_aip = ax.plot(x, spc.airy(x)[2], label='$\mathrm{Bi}(x)$')

plt.ylim(ymin=-.5, ymax=1.)
plt.legend(loc='upper right', prop={'size':12})
plt.xlabel('$x$')
plt.grid()

plt.gcf().set_size_inches(8, 3.8)

plt.savefig('airy_bairy.pdf', bbox_inches='tight')
