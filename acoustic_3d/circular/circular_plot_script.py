from circular_trapped import Circ_trapped
from circular_spectral import Circ_spectral
import scipy as sp
import matplotlib as mpl
import pylab as pl
import numpy as np
import time
ct = Circ_trapped()
cs = Circ_spectral()

ploth1 = False

if ploth1:
	ct.set_params(h1=sp.linspace(1.005,1.995,100),p=40,multiple_p=True)
	cs.set_params(h1=sp.linspace(1.005,1.995,10),R=15,X=30)

	print "Calculating symmetric wavenumbers (asymptotic)..."
	t = time.time()
	ct.k_h1()
	print "{0} sec elapsed".format(time.time() - t)
	trappedSym = ct.k_result_h1

	ct.set_params(symm=False)
	print "Calculating antisymmetric wavenumbers (asymptotic)..."
	t = time.time()
	ct.k_h1()
	print "{0} sec elapsed".format(time.time() - t)
	trappedAnSym = ct.k_result_h1

	print "Calculating symmetric wavenumbers (spectral)..."
	t = time.time()
	cs.k_h1()
	print "{0} sec elapsed".format(time.time() - t)
	spectSym = cs.k_result_h1

	cs.set_params(symm=False)
	print "Calculating antisymmetric wavenumbers (spectral)..."
	t = time.time()
	cs.k_h1()
	print "{0} sec elapsed".format(time.time() - t)
	spectAnSym = cs.k_result_h1

else:
	ct.set_params(eps=sp.linspace(0.005,0.995,100),p=3,multiple_p=True)
	ct.set_params(eps=sp.linspace(0.005,0.995,100),p=3)
	cs.set_params(eps=sp.linspace(0.005,0.995,10),R=15,X=35)

	ct.k_eps()
	trappedSym = ct.k_result_eps
	cs.k_eps()
	spectSym = cs.k_result_eps[:,0:3]

	ct.set_params(symm=False)
	cs.set_params(symm=False)

	ct.k_eps()
	trappedAnSym = ct.k_result_eps
	cs.k_eps()
	spectAnSym = cs.k_result_eps[:,0:3]

trapped = np.hstack([trappedSym,trappedAnSym])
s = trapped[-1,:].argsort()
trapped = trapped[:,s]

spectral = np.hstack([spectSym,spectAnSym])
s = spectral[-1,:].argsort()
spectral = spectral[:,s]

c1 = '#691909'
c2 = '#71BAEB'

mpl.rc('text', usetex='true')
mpl.rc('font', family='serif')
mpl.rc('font', serif=[])

#cmap = np.tile(np.vstack([c1, c2]),(trapped[0].size,1))
cmap = np.array([c1, c2])

pl.rcParams['mathtext.default'] = 'regular'

for i in range(trapped[0].size):
	if ploth1:
		pl.plot(ct.h1,trapped[:,i],c=cmap[i % 2], ls='-')
	else:
		pl.plot(ct.eps,trapped[:,i],c=cmap[i % 2], ls='-')

for i in range(spectral[0].size):
	if ploth1:
		pl.plot(cs.h1,spectral[:,i],'x',c=cmap[i % 2])
	else:
		pl.plot(cs.eps,spectral[:,i],'x',c=cmap[i % 2])

lns = pl.gca().get_lines()
l1 = lns[0]
l2 = lns[1]
l3 = lns[trapped[0].size]
l4 = lns[trapped[0].size+1]

pl.legend( (l1,l2,l3,l4), ('(S) Asymptotic','(A) Asymptotic','(S) Spectral','(A) Spectral'),numpoints=1,loc='upper right',prop={'size':12})

if ploth1:
	pl.xlabel('$h_1$')
else:
	pl.xlabel('$\epsilon$')
pl.ylabel('$k$')

pl.grid()
pl.gcf().set_size_inches([8,6])
#pl.savefig('circular.pdf', bbox_inches='tight')
