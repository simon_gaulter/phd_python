# -*- coding: utf-8 -*-
"""
@author: Simon Gaulter
"""
import scipy as sp
import scipy.special as spc
import scipy.integrate as spi
import scipy.optimize as spo

class Circ_trapped:
	"""Provides methods to calculate the eigenvalues of the circular waveguide.
	set_params  -- method to set the required parameters;
	get_params  -- return current parameters;
	k           -- return the trapped mode wavenumber for the current parameters;
	
	Optional function handle, bulge height, small parameter and mode numbers can
	be passed via set_params(), or as keyword arguments when calling k_root().
	"""

	SOUNDSOFT = 'soundsoft'
	SOUNDHARD = 'soundhard'
	SYMMETRIC = 'symmetric'
	ANTISYMMETRIC = 'antisymmetric'

	def __init__(self):
		""" Initialise the class and set parameters.
		Defaults to finding a single k."""
		self.set_params(eps=0.1,h1=1.5,m=1,n=1,p=1,\
				symm=self.SYMMETRIC,bc=self.SOUNDSOFT)
		self.get_params()

	def get_params(self):
		"""Return the current parameters."""
		print "Current parameters:"
		print "eps:        " + str(self.eps)
		print "h1:         " + str(self.h1)
		print "m:          " + str(self.m)
		print "n:          " + str(self.n)
		print "p:          " + str(self.p)
		print "symm:       " + str(self.symm)
		print "bc:         " + str(self.bc)

	def set_params(self,eps=None,h1=None,m=None,n=None,\
			p=None,h=None,symm=None,bc=None):
		"""Optionally change the parameters. These can also be passed
		as keyword arguments as the function k_root is called.

		Keyword arguments:
			eps        -- one or more values of epsilon to use
			h1         -- one or more values of h1 to use
			m          -- azimuthal mode
			n          -- radial mode
			p          -- longitudinal mode
			symm       -- longitudinal symmetry
			              either self.SYMMETRIC or self.ANTISYMMETRIC
			bc         -- boundary condition
			              either self.SOUNDSOFT or self.SOUNDHARD
			h          -- pass a custom function handle
			              default is 1 + (h1 - 1) * sech(x)"""
		if m is not None:
			# mth azimuthal mode
			self.m = m
		if n is not None:
			# nth radial mode
			self.n = n
		if p is not None:
			# pth longitudinal mode
			self.p = p
		if symm is not None:
			# Either symmetric or antisymmetric modes
			self.symm = symm
		if bc is not None:
			# sound hard / sound soft waveguide
			self.bc = bc
		if h1 is not None:
			# h1 max bulge height
			self.h1 = h1
		if eps is not None:
			# 0 < eps << 1
			self.eps = eps
		if h is not None:
			# a custom function handle for the boundary profile
			self.__hcustom = h

		# Get the mth root of the nth bessel function
		if self.bc == self.SOUNDHARD:
			self.bess = spc.jnp_zeros(self.m,self.n)[-1]
		else:
			self.bess = spc.jn_zeros(self.m,self.n)[-1]

		# Get the pth root of either Ai (antisymmetric)
		# or Ai' (symmetric)
		if self.symm == self.SYMMETRIC:
			self.aiz = spc.ai_zeros(self.p)[1][-1]
		else:
			self.aiz = spc.ai_zeros(self.p)[0][-1]

	def k(self,**kwargs):
		"""Find a single trapped mode wavenumber."""
		self.set_params(**kwargs)
		I = lambda k: -self.g(0.,k) + self.eps**(2./3)*self.aiz
		# Find the root - Brent algorithm
		try:
			k = spo.brentq(I,self.bess/self.h1 \
					+ 1.E-12,self.bess - 1.E-12, maxiter=500)
		except:
			k = sp.nan
		return k

	def g(self,x,k):
		"""Return the phase function, g(x,k).
		Takes only positive values of x."""
		it = sp.nditer([x,None])
		for xx,y in it:
			if xx < self.xstar(k):
				y[...] = -(3./2 * spi.quad(\
						lambda x: sp.sqrt(k**2 - (self.bess/self.h(x))**2),\
						xx, self.xstar(k))[0])**(2./3)
			else:
				y[...] = (3./2 * spi.quad(\
						lambda x: sp.sqrt((self.bess/self.h(x))**2 - k**2),\
						self.xstar(k), xx)[0])**(2./3)
		return it.operands[1]

	def xstar(self,k):
		"""Return the turning point, x_star."""
		return sp.arccosh(k*(self.h1 - 1.)/(self.bess - k))

	def h(self,x):
		"""Return the height function, h(x)."""
		try:
			return self.__hcustom(x)
		except:
			return self.__h(x)

	def __h(self,x):
		"""The default height function"""
		return 1. + (self.h1 - 1.)/sp.cosh(x)

	def tau(self,x):
		"""Return the transverse eigenvalue, tau_mn(x)."""
		return self.bess/self.h(x)
