from circular_spectral import Circ_spectral
import numpy as np
cs = Circ_spectral()

X = [ n * 5 for n in range(2,13) ]
R = [ n * 5 for n in range(2,6) ]

r = np.zeros([len(X), len(R)])

for i in range(0,len(X)):
	 for j in range(0,len(R)):
		cs.set_params(X=X[i],R=R[j])
		cs.k_h1()
		r[i][j] = cs.k_result_h1[0][5]

print r
#np.savetxt('convergence.csv', r, delimiter=',')
