import mayavi.mlab as m
from circular_trapped import Circ_mesh

c = Circ_mesh()

n = [1, 2, 3, 4]
p = [1, 2, 3, 4]

c.set_params(soundhard=True)

for ni in n:
	for pi in p:
		print "Plotting n = {0}, p = {1} (antisymmetric)...".format(*[ni,pi])
		c.set_params(n=ni,p=pi,symm=False)
		m.clf()
		c.plot_waveguide_mesh()
		c.plot_phi_mesh()
		m.savefig('solution_plots/neumann/asymm_n{0}p{1}.png'.format(*[ni,pi]), size=[1280,720])

		print "Plotting n = {0}, p = {1} (symmetric)...".format(*[ni,pi])
		c.set_params(n=ni,p=pi,symm=True)
		m.clf()
		c.plot_waveguide_mesh()
		c.plot_phi_mesh()
		m.savefig('solution_plots/neumann/symm_n{0}p{1}.png'.format(*[ni,pi]), size=[1280,720])
