import scipy as sp
import scipy.special as spc
import mayavi.mlab as ml

r = sp.linspace(0, 1, 100)
theta = sp.linspace(0, 2 * sp.pi, 100)

rr, ttheta = sp.meshgrid(r, theta)

fig = ml.gcf()
ml.savefig('m-n-.png', figure=fig, size=[1280,720])
for m in range(0,3):
	for n in range(1,4):

		z = 0.5 * sp.cos(m * ttheta) * spc.jn(m, spc.jn_zeros(m, n)[-1] * rr)

		lim = max(abs(z.min()),abs(z.max()))

		x = rr * sp.cos(ttheta)
		y = rr * sp.sin(ttheta)

		ml.clf(fig)
		ml.mesh(x, y, z, figure=fig,vmin=-lim,vmax=lim)
		ml.view(azimuth=110,elevation=60,distance=5)
		ml.savefig('m{0}n{1}.png'.format(*[m,n]), figure=fig, size=[1280,720])
#ml.roll(-110)
