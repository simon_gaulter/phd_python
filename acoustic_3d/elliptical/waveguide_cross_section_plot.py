import numpy as np
import scipy as sp
import matplotlib as mpl
import matplotlib.pyplot as plt

mpl.rc('text', usetex='true')
mpl.rc('font', family='serif')
mpl.rc('font', serif=[])

def rho(xi, theta, h1):
	r = (2 + 2 * (h1 - 1) * (1 / sp.cosh(xi)) * sp.exp(-16 * sp.sin(theta / 2 - np.pi / 4)**2) ) \
			/ ( sp.sqrt(sp.cos(theta)**2 + 4 * sp.sin(theta)**2) )
	return r

theta = np.linspace(0, 2*np.pi, 100, endpoint=True)

ax1 = plt.subplot(121,polar=True)
ax1.plot(theta, rho(0, theta, 1.5))
ax2 = plt.subplot(122,polar=True)
ax2.plot(theta, rho(100, theta, 1.5))

#plt.show()
plt.gcf().set_size_inches(8, 4)
plt.savefig('ellipse_cross_section.pdf', bbox_inches='tight')
