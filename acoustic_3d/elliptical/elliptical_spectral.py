import numpy as np
import scipy as sp
import scipy.special as spc
import scipy.linalg as spl
import pylab as pl

class Ellipse_spectral:
    # TODO - docstring

    def __init__(self):
        self.set_params(h1=1.5,eps=0.1,m=1,n=1,symm=True,X=20,R=20)

    def set_params(self,h1=None,eps=None,m=None,n=None,symm=None,X=None,R=None):
        if h1 is not None:
            if isinstance(h1,np.ndarray):
                self.h1 = h1
            else:
                self.h1 = np.array([h1])
        if eps is not None:
            if isinstance(eps,np.ndarray):
                self.eps = eps
            else:
                self.eps = np.array([eps])
        if m is not None:
            self.m = m
        if n is not None:
            self.n = n
        if symm is not None:
            self.symm = symm
        if X is not None:
            self.X = X
        if R is not None:
            self.R = R

    def get_params(self):
        """Return the current parameters."""
        print "h1:         " + str(self.h1)
        print "eps:        " + str(self.eps)
        print "m:          " + str(self.m)
        print "n:          " + str(self.n)
        print "symm:       " + str(self.symm)
        print "X-by-R:     " + str(self.X) + "-by-" + str(self.R)

    def plot_k_result(self, kind=None, title=True, show=True):
        """kind can be 'eps' or 'h1', to specifiy result type.
        If none is specified, use the last created k_result.
        title is a boolean which determines if the plot title is printed."""
        
        if kind == 'h1':
            pl.plot(self.h1,self.k_result_h1,'x')
            xl = '$h_1$'
            t = '$\epsilon = ' + str(self.eps[0]) + '$'
        elif kind == 'eps':
            pl.plot(self.eps,self.k_result_eps,'x')
            xl = '$\epsilon$'
            t = '$h_1 = ' + str(self.h1[0]) + '$'
        else:
            if hasattr(self,'lastcalled') and self.lastcalled == 'k_eps':
                pl.plot(self.eps,self.k_result_eps,'x')
                xl = '$\epsilon$'
                t = '$h_1 = ' + str(self.h1[0]) + '$'
            elif hasattr(self,'lastcalled') and self.lastcalled == 'k_h1':
                pl.plot(self.h1,self.k_result_h1,'x')
                xl = '$h_1$'
                t = '$\epsilon = ' + str(self.eps[0]) + '$'
            else:
                print "No data to plot. Call method 'k_eps' or 'k_h1' first."
                return

        pl.rcParams['mathtext.default'] = 'regular'
        if title == True:
            pl.title('Wavenumbers $k$ against ' + xl + ' for $m = '\
                    + str(self.m) + '$, $n = ' + str(self.n) + '$ and ' + t + '.')
        pl.xlabel(xl)
        pl.ylabel('$k$')
        if show == True:
            pl.show()

    def k_h1(self):
        eig = np.empty([self.h1.size,(self.R-1)*(self.X-1)])
        for i,h in enumerate(self.h1):
            eig[i,:] = self.spectral(h1=h)
        eig[eig>spc.jn_zeros(self.m,self.n)[-1]] = sp.nan
        eig = eig[:,np.any(~np.isnan(eig),axis=0)]
        self.k_result_h1 = eig
        self.lastcalled = 'k_h1'

    def k_eps(self):
        eig = np.empty([self.eps.size,(self.R-1)*(self.X-1)])
        for i,e in enumerate(self.eps):
            eig[i,:] = self.spectral(eps=e)
        eig[eig>spc.jn_zeros(self.m,self.n)[-1]] = sp.nan
        eig = eig[:,np.any(~np.isnan(eig),axis=0)]
        self.k_result_eps = eig
        self.lastcalled = 'k_eps'

    def spectral(self,h1=None,eps=None):
        """Calculate the Helmholtz differentiation matrix
        and return the eigenvalues."""
        N = self.R
        M = self.X
        if h1 is None:
            h1 = self.h1
        if eps is None:
            eps = self.eps
        # Chebyshev matrix in the r-direction.
        E,r = self.cheb_mat()
        E2 = np.dot(E,E)[1:,1:]
        E1 = E[1:,1:]
        r = r[1:]

        # Laguerre matrix in the x-direction.
        if self.symm == False:
            # Dirichlet boundary for antisymmetric modes.
            D,x = self.lag_mat(b=2)
            D2 = np.dot(D,D)[1:,1:]
            D1 = D[1:,1:]
            x = x[1:]
        else:
            # Neumann boundary for symmetric modes.
            D,x = self.lag_mat(b=2)
            D2 = np.dot(D,D)
            D11 = D[0,0]
            DR1 = D[0,1:]
            DC1 = D[1:,0][np.newaxis].T
            DC2 = D2[1:,0][np.newaxis].T
            
            D2 = D2[1:,1:]
            D = D[1:,1:]
            x = x[1:]
            D1 = D - (1./D11) * DC1*DR1
            D2 = D2 - (1./D11) * DC2*DR1

        xx,rr = np.meshgrid(x,r)
        xx = xx.flatten(1)
        rr = rr.flatten(1)

        Ir = np.eye(x.size)
        Ix = np.eye(r.size)
        
        DXX = np.kron(D2,Ix)
        DR = np.kron(Ir,E1)
        DRR = np.kron(Ir,E2)
        DXR = np.kron(D1,E1)

        xe = eps * xx
        h = 1 + (h1 - 1)/np.cosh(xe)
        dh = eps * (1 - h1)*np.tanh(xe)/np.cosh(xe)
        ddh = eps**2. * (1. - h1) * (1./np.cosh(xe)) * (2./(np.cosh(xe)**2.) - 1)
        drhox = -(rr + 1) * dh / h
        drhoxx = 2.*(rr + 1) * (dh**2.)/(h**2.) - (rr + 1)*ddh/h
        drhor = 2./h

        rho = (rr + 1)
        
        HH = -np.dot(np.diag(rho**2.),DXX) \
        - np.dot(np.diag(rho**2.*(drhox**2. + drhor**2.)),DRR) \
        - np.dot(np.diag(2*rho**2.*drhox),DXR) \
        - np.dot(np.diag(2 * rho * drhor / h + rho**2. * drhoxx),DR) \
        + np.dot(np.diag(4*self.m**2/(h**2)),np.eye(DR[0].size))

        B = np.diag(rho**2.)

        return np.sqrt(np.sort(np.real(spl.eig(HH,B)[0])))



    def cheb_mat(self):
        if self.R == 0:
            self.D = 0
            self.r = 1
            return
        else:
            N = self.R - 1

        x = sp.cos(sp.pi*(np.arange(0,N+1)/float(N))).T

        c = np.vstack([[2],np.ones([N-1,1]),[2]]).conj().T*(-1.)**np.arange(0,N+1)

        X = np.tile(x,(N+1,1)).T
        dX = X - X.T
        D = (c*((1./c).T)/(dX+(np.eye(N+1))))
        D = D - np.diag(np.sum(D,axis=0))
        self.cheb_D = -D.T
        self.cheb_r = x
        return self.cheb_D,self.cheb_r
    
    def lag_mat(self,b=1):
        M = self.X
        x = self.__lag_roots(M-1)
        x = np.insert(x,0,0.)
        alpha = sp.exp(-x/2.)
        
        beta = (-.5)*np.ones(x.size)
        
        self.lag_D = b*self.__poldif(x,alpha,beta)
        
        self.lag_x = x/b

        return self.lag_D,self.lag_x

    def __lag_roots(self,M):
        J = np.diag(np.arange(1,2*M,2)) - np.diag(np.arange(1,M),1)\
        - np.diag(np.arange(1,M),-1)
        return np.sort(np.real(spl.eig(J)[0]))

    def __poldif(self,x,malpha,B):
        N = x.size
        
        x = x[np.newaxis].T
        
        alpha = malpha.reshape([malpha.size,1])
        
        L = np.eye(N,dtype=bool)
        
        DX = x - x.T
        DX[L] = 1
        
        c = alpha*np.prod(DX,axis=1).reshape([N,1])
        C = c/c.T

        Z = 1./DX
        Z[L] = 0

        X = Z[~L].reshape(N-1,N,order='F')

        Y = np.ones([N-1,N])
        D = np.eye(N)
        
        Y = np.cumsum(np.vstack([B, Y[0:N-1,:]*X]),axis=0)
        D = Z*(C*np.tile(np.diag(D),(N,1)) - D)
        D[L] = Y[N-1,:]
        return D
