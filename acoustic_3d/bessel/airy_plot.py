import scipy as sp
import scipy.special as spc
import matplotlib as mpl
import matplotlib.pyplot as plt

x = sp.linspace(-8, 5, 1000)

mpl.rc('text', usetex='true')
mpl.rc('font', family='serif')
mpl.rc('font', serif=[])

ax = plt.axes()

line_ai = ax.plot(x, spc.airy(x)[0], label='$\mathrm{Ai}(x)$')
line_aip = ax.plot(x, spc.airy(x)[1], label='$\mathrm{Ai\'}(x)$')

z, zp = spc.ai_zeros(5)[0:2]

pts_zp = ax.plot(zp, sp.zeros(len(z)), 'go')
pts_z = ax.plot(z, sp.zeros(len(z)), 'bo')

for i in range(0,len(z)):
	plt.annotate(s='$z_{' + str(i + 1) + '}$', xy=(z[i],0), xytext=(0,-14), textcoords='offset points', size=12, bbox=dict(boxstyle='round,pad=0', fc='white', ec='white', alpha=0.8))

for i in range(0,len(zp)):
	plt.annotate(s='$z_{' + str(i + 1) + '}\'$', xy=(zp[i],0), xytext=(0,14), textcoords='offset points', size=12, bbox=dict(boxstyle='round,pad=0', fc='white', ec='white', alpha=0.8))

plt.xlim(xmax=5)

plt.legend(loc='upper right', prop={'size':12})
plt.xlabel('$x$')

plt.grid()
plt.gcf().set_size_inches(8, 5.8)
plt.savefig('airy_airyp.pdf', bbox_inches='tight')
