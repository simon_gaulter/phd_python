import scipy as sp
import scipy.special as spc
import matplotlib as mpl
import matplotlib.pyplot as plt

x = sp.linspace(-15, 15, 1000)

mpl.rc('text', usetex='true')
mpl.rc('font', family='serif')
mpl.rc('font', serif=[])

ax = plt.axes()

J0 = ax.plot(x, spc.jvp(0,x), label='$\mathrm{J}_0\'(x)$')
J1 = ax.plot(x, spc.jvp(1,x), label='$\mathrm{J}_1\'(x)$')
J2 = ax.plot(x, spc.jvp(2,x), label='$\mathrm{J}_2\'(x)$')
J3 = ax.plot(x, spc.jvp(3,x), label='$\mathrm{J}_3\'(x)$')

plt.ylim(ymin=-1.1, ymax=1.1)
plt.legend(loc='upper right', prop={'size':12})
plt.xlabel('$x$')
plt.grid()

plt.gcf().set_size_inches(8, 3.8)

#plt.ion()
#plt.show()
plt.savefig('bessel_d_curve.pdf', bbox_inches='tight')
