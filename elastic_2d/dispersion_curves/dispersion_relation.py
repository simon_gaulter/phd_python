# -*- coding: utf-8 -*-
import numpy as np
import scipy as sp
import matplotlib as mpl
import matplotlib.pyplot as pl
from numpy.lib.scimath import sqrt as csqrt

class Dispersion(object):

	STRESSFREE = 'stress_free'
	RIGID = 'rigid'
	SYMMETRIC = 'symmetric'
	ANTISYMMETRIC = 'antisymmetric'

	def __init__(self):
		self.poisson = 0.3
		self.x_range = np.arange(0.001, 20.001, .005)
		self.y_range = np.arange(0.001, 5.001, .005)

		self.k_range = np.linspace(0, 6.0, 1000)
		self.p_range = np.linspace(0, 3.0, 1000)
		self.w = 2.0

	def gamma(self,nu=None):
		if nu is None:
			nu = self.poisson
		return csqrt( (1. - 2. * nu) / (2. - 2. * nu) )

	def dispersion1(self, x, y, symm=False):
		# Dispersion relation for iω /(c_ψ p') against ω w/c_ψ

		gamma = self.gamma()

		tauphi = csqrt(x**2. / y**2. - x**2. * gamma**2.) / 2.
		taupsi = csqrt(x**2. / y**2. - x**2.) / 2.

		if symm:
			return (x**2. / (2. * y**2.) - x**2. / 4.)**2. * np.cosh(tauphi) * np.sinh(taupsi) \
					- (x**2. / y**2.) * tauphi * taupsi * np.sinh(tauphi) * np.cosh(taupsi)
		else:
			return (x**2. / (2. * y**2.) - x**2. / 4.)**2. * np.sinh(tauphi) * np.cosh(taupsi) \
					- (x**2. / y**2.) * tauphi * taupsi * np.cosh(tauphi) * np.sinh(taupsi)

	def curves1(self, x_range=None, y_range=None, **kwargs):
		if x_range is None: x_range = self.x_range
		if y_range is None: y_range = self.y_range

		x_grid = np.expand_dims(x_range, axis = 0)
		y_grid = np.expand_dims(y_range, axis = 1)

		self.zA = np.real(self.dispersion1(x_grid, y_grid, symm=False)) \
				+ np.imag(self.dispersion1(x_grid, y_grid, symm=False))
		self.zS = np.real(self.dispersion1(x_grid, y_grid, symm=True)) \
				+ np.imag(self.dispersion1(x_grid, y_grid, symm=True))

		#pl.clf()
		pl.ion()
		self.ax = pl.subplot(111)

		self.cA = self.ax.contour(x_range, y_range, self.zA, [0], colors='r', label='Antisymmetric',antialiased=True,**kwargs)
		self.cS = self.ax.contour(x_range, y_range, self.zS, [0], colors='b', label='Symmetric',antialiased=True,**kwargs)

		pl.rc('text', usetex='true')
		mpl.rc('font', family='serif')
		mpl.rc('font', serif=[])
		pl.rc('font', size=14)
		
		pl.grid()

		pl.xticks(np.linspace(np.floor(x_range[0]), np.round(x_range[-1]), 11))
		pl.yticks(np.linspace(np.floor(y_range[0]), np.round(y_range[-1]), 11))
		pl.xlabel(r"$\displaystyle{\frac{w \omega}{c_{\psi}}}$")
		pl.ylabel(r"$\displaystyle{\frac{\omega}{i c_{\psi} p'}}$")
		#pl.title(r"Dispersion curves of phase velocity against width--frequency product for poission ratio $\nu = {0}$".format(self.poisson), y=1.03)
		#pl.show()
		#pl.savefig('disp1.pgf', bbox_inches='tight')

	def to_real(self, x):
		return np.real(x) + np.imag(x)

	def dispersion_kp(self, k, p, boundary=None, symm=True):
		
		bc = boundary if boundary is not None else self.STRESSFREE
		gamma = self.gamma()
		w = self.w

		tphi = w / 2. * np.sqrt(p**2. + gamma**2. * k**2.)
		tpsi = w / 2. * np.sqrt(p**2. + k**2.)

		if symm is self.SYMMETRIC:
			a = sp.cos
			b = sp.sin
		elif symm is self.ANTISYMMETRIC:
			a = sp.sin
			b = sp.cos

		if bc == self.STRESSFREE:
			return (w**2. / 4. * (2. * p**2. + k**2.))**2. * a(tphi) * b(tpsi) \
					- w**2. * p**2. * tphi * tpsi * a(tpsi) * b(tphi)
		else:
			return w**2. * p**2. * a(tphi) * b(tpsi) - 4. * tpsi * tphi * a(tpsi) * b(tphi)

	def curves_kp(self, k_range=None, p_range=None, symm=None, boundary=None, **kwargs):
		symm = symm if symm is not None else self.SYMMETRIC
		if k_range is None: k_range = self.k_range
		if p_range is None: p_range = self.p_range

		kk,pp = np.meshgrid(k_range,p_range)
		col = 'r' if symm == self.ANTISYMMETRIC else 'b'
		disp = self.to_real(self.dispersion_kp(kk,1j*pp,symm=symm,boundary=boundary))
		con = pl.contour(kk, pp, disp,[0],colors=col,**kwargs)
		con.collections[0].set_label(symm.title())
		pl.grid(True)
		pl.rc('text', usetex=True)
		pl.rc('font', family='serif')
		pl.rc('font', serif=['Computer Modern Roman'])
		pl.rc('font', size=14)
		pl.xlabel(r"$\displaystyle{\frac{\omega}{c_{\psi}}}$")
		pl.ylabel(r"$i p'$")
		#pl.legend(loc='upper left')
		pl.ion()
		pl.show()

#d = Dispersion()
#d.poisson = 0.3
#d.curves1()
#d.poisson = 0.4999
#d.curves1(linestyles=':')
#d.ax.legend()
