import numpy as np
import matplotlib.pyplot as plt
from numpy.lib.scimath import sqrt as csqrt
def dispers(cT,gamma,w,lns):
	global ax
	# We define kappa to be i*f', so that we find propagating modes.
	omega,kappa = np.mgrid[1:4+.01:.01,0:2+.01:.01]
	
	# Propagating
	
	nuL = csqrt( (w/2)**2 * ( -kappa**2 + (gamma * omega/cT)**2 ) )
	nuT = csqrt( (w/2)**2 * ( -kappa**2 + (omega/cT)**2 ) )
	
	# Symmetric
	
	zS = -(w*kappa/2)**2*np.cos(nuL)*np.sin(nuT) - nuL*nuT*np.sin(nuL)*np.cos(nuT)
	cs = ax.contour(omega,kappa,zS, [0], colors='b', antialiased=True, linestyles=lns)

	# Antisymmetric
	zA = -(w*kappa/2)**2*np.cos(nuT)*np.sin(nuL) - nuL*nuT*np.sin(nuT)*np.cos(nuL)
	ca = ax.contour(omega,kappa,zA, [0], colors='r', antialiased=True, linestyles=lns)

cT = 1			# Transverse wavespeed
gamma = .547	# Ratio cT/cL

plt.figure()
ax = plt.subplot(1,1,1)

dispers(cT,gamma,2.05,'--')
dispers(cT,gamma,2,'-')
dispers(cT,gamma,1.95,':')

plt.rcParams['mathtext.default'] = 'regular'

#plt.legend(('S, w=2', 'A, w=2','S, w=2.05','A, w=2.05','S, w=1.95','A, w=1.95'),loc='upper left')

plt.title("Dispersion curves for $\gamma = 0.547$ and varying duct widths, $w$.\nModes symmetric and antisymmetric about $\eta=0$ shown. ")
plt.xlabel(r"$\omega$", fontsize=14)
plt.ylabel(r"$f'$", fontsize=14)
plt.grid()
plt.savefig('dispersion')
plt.show()
