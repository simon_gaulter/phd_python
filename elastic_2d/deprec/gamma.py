import numpy as np
import scipy.optimize as sciopt
import matplotlib.pyplot as plt
from numpy.lib.scimath import sqrt as csqrt

class Gamma(object):

    def __init__(self):
        self.nuL = self.NuL(self)
        self.nuT = self.NuT(self) 
        self._omega = np.arange(1, 4+.01, .01)
        self._cT = 1.
        self._gamma = .547
        self._h0 = 1.
        self._h1 = 1.5

    @property
    def omega(self):
        return self._omega

    @omega.setter
    def omega(self, value):
        self._omega = value

    @property
    def cT(self):
        return self._cT

    @cT.setter
    def cT(self, value):
        self._cT = value

    @property
    def gamma(self):
        return self._gamma

    @gamma.setter
    def gamma(self, value):
        self._gamma = value
    
    @property
    def h0(self):
        return self._h0

    @h0.setter
    def h0(self, value):
        self._h0 = value

    @property
    def h1(self):
        return self._h1

    @h1.setter
    def h1(self, value):
        self._h1 = value

    def w(self, xi):
        return 2 * (self.h0 + (self.h1 - self.h0) / np.cosh(xi))

    def wd(self, xi):
        return 2 * (self.h0 - self.h1) * np.tanh(xi) / np.cosh(xi)

    def _turning_point(self, omega, bNu_range, nu_fn, n, symm = True):
        """Calculate the nth turning point, i.e., where 
        nuL_n^2 + (w * gamma * kT_n / 2)^2 = 0 and
        nuT_n^2 + (w * kT_n / 2)^2 = 0
        (these should be equal)."""
        
        # get range of xi
        
        if nu_fn.__name__ is "nuL":
            tp_rel = lambda xi : (nu_fn(omega, bNu_range, xi, symm)[n]**2 \
                    + (self.w(xi) * self.gamma * omega / (2. * self.cT))**2)
        elif nu_fn.__name__ is "nuT":
            tp_rel = lambda xi : (nu_fn(omega, bNu_range, xi, symm)[n]**2 \
                    + (self.w(xi) * omega / (2. * self.cT))**2)

        # - just plotting here to get an idea of its shape
        ########################
        #xi = np.arange(0,10.1,.1)
        #tp = np.array([])
        #for idx,x in enumerate(xi):
            #tp = np.append(tp, tp_rel(x))
        #plt.plot(xi, tp)
        #plt.show()
        ########################
        
        # 710 is the maximum argument of np.cosh on this machine;
        # i.e., np.cosh(711) = inf.
        try:
            return sciopt.brentq(tp_rel, 0, 710)
        except IndexError as e:
            print "nu_" + str(n), "does not exist within that range."
            return None
        except ValueError as e:
            print "No turning point found."
            return None

    def _disp_rel_plot(self, omega, bNu_range, xi, disp_fn, symm = True):
        """Utility function to plot the dispersion relation,
        D, for a SINGLE omega and range of bNuL or bNuT.

        bNuL or bNuT are the zeroes of this plot.

        This should not be called directly, but from the inner
        classes NuL, NuT. """
        disp_rel = disp_fn(omega, bNu_range, self.w(xi), symm)
        plt.ion()
        plt.grid(True)
        plt.plot(np.expand_dims(bNu_range, axis=1), disp_rel) 
        plt.show()

    def _disp_rel_root_bNu(self, omega, bNu_range, xi, disp_fn, symm = True):
        """Calculate bNu = -i*nu as a root of the dispersion
        relation, for a SINGLE omega."""
        
        disp_rel = lambda nu : disp_fn(omega, nu, xi, symm)
        roots = np.array([])
        for i in range(bNu_range.size - 1):
            try:
                root = sciopt.brentq(disp_rel, bNu_range[i], bNu_range[i+1])
            except ValueError as e:
                pass
            else:
                roots = np.append(roots, root)

        return roots

    class NuT(object):

        def __init__(self, outer):
            self.outer = outer
            self._bNuT_range = np.arange(0, 10+.1, .1)

        @property
        def bNuT_range(self):
            return self._bNuT_range

        @bNuT_range.setter
        def bNuT_range(self, value):
            self._bNuT_range = value
        
        def turning_point(self, omega, bNuT_range, n, symm = True):
            return self.outer._turning_point(omega, bNuT_range, self.nuT, n, symm)
        
        def disp_rel(self, omega_range, bNuT_range, xi, symm = True):
            """Calculate dispersion relation, D, for a given range
            of bNuT in R, omega in R and single xi.

            Assumes nuT = i*bNuT for nuT in iR.
            Use broadcasting to calculate over a range.

            This method is passed to the outer class's methods to calculate
            roots or plot bNu."""

            w = self.outer.w(xi)

            omega = np.expand_dims(omega_range, axis=0)
            
            bNuT = np.expand_dims(bNuT_range, axis=1)
            
            kT = omega / self.outer.cT
            bNuL = csqrt(bNuT**2 + w**2 * (self.outer.gamma**2 - 1) * kT**2 / 4.)
            wf2 = (kT * w)**2 / 4. - bNuT**2

            # np.real is required here to avoid
            # TypeError: can't convert complex to float
            if symm:
                return np.real(wf2 * np.sin(bNuT) * np.cos(bNuL) \
                        + bNuT * bNuL * np.sin(bNuL) * np.cos(bNuT))
            else:
                return np.real(wf2 * np.sin(bNuL) * np.cos(bNuT) \
                        + bNuL * bNuT * np.sin(bNuT) * np.cos(bNuL))

        def disp_rel_plot(self, omega, bNuT_range, xi, symm = True):
            self.outer._disp_rel_plot(omega, bNuT_range, \
                    self.outer.w(xi), self.disp_rel, symm)

        def nuT(self, omega, bNuT_range, xi, symm = True):
            """Find a nuT in iR as a root of the dispersion relation
            for a given range in iR. I.e., for i * bNuT = nuT."""
            #FIXME? - nuT = 0 is always a root - we'll throw this away for now.
            return self.outer._disp_rel_root_bNu(omega, bNuT_range, \
                    xi, self.disp_rel, symm)[1:] * 1j

    class NuL(object):

        def __init__(self, outer):
            self.outer = outer
            self._bNuL_range = np.arange(0, 10+.1, .1)

        @property
        def bNuL_range(self):
            return self._bNuL_range

        @bNuL_range.setter
        def bNuL_range(self, value):
            self._bNuL_range = value

        def turning_point(self, omega, bNuL_range, n, symm = True):
            return self.outer._turning_point(omega, bNuL_range, self.nuL, n, symm)
        
        def disp_rel(self, omega_range, bNuL_range, xi, symm = True):
            """Calculate dispersion relation, D, for a given range
            of bNuL in iR, omega in R and single xi.

            Assumes nuL = i*bNuL for nuL in iR.
            Use broadcasting to calculate over a range.

            This method is passed to the outer class's methods to calculate
            roots or plot bNu."""

            w = self.outer.w(xi)
            
            omega = np.expand_dims(omega_range, axis=0)
            
            bNuL = np.expand_dims(bNuL_range, axis=1)
            
            kT = omega / self.outer.cT
            bNuT = csqrt(bNuL**2 + w**2 * (1 - self.outer.gamma**2) * kT**2 / 4.)
            wf2 = (self.outer.gamma * kT * w)**2 / 4. - bNuL**2

            if symm:
                return wf2 * np.sin(bNuT) * np.cos(bNuL) \
                        + bNuT * bNuL * np.sin(bNuL) * np.cos(bNuT)
            else:
                return wf2 * np.sin(bNuL) * np.cos(bNuT) \
                        + bNuL * bNuT * np.sin(bNuT) * np.cos(bNuL)

        def disp_rel_plot(self, omega, bNuL_range, xi, symm = True):
            self.outer._disp_rel_plot(omega, bNuL_range, \
                    self.outer.w(xi), self.disp_rel, symm)

        def nuL(self, omega, bNuL_range, xi, symm = True):
            """Find MULTIPLE nuL in iR as a root of the dispersion relation
            for a given range in iR. I.e., for i * bNuL = nuL."""
            return self.outer._disp_rel_root_bNu(omega, bNuL_range, \
                    xi, self.disp_rel, symm) * 1j

        def nuL_dash(self, omega, nuL, xi, symm = True):
            """Calculate nuL' for a SINGLE omega and nuL."""

            kT = omega / self.outer.cT

            w = self.outer.w(xi)
            wd = self.outer.wd(xi)

            S = csqrt((self.outer.gamma**2 - 1) * (w * kT)**2 + 4. * nuL**2)
            T = 2. * (self.outer.gamma**2 - 1) * w * wd * kT**2

            A = - (S * np.cosh(S / 2.) * np.sinh(nuL) / 2.) \
                    + (nuL * np.cosh(S / 2.) * np.cosh(nuL) * (w * self.outer.gamma * kT)**2 / (2. * S)) \
                    + (np.sinh(S / 2.) * np.sinh(nuL) * (w * self.outer.gamma * kT)**2 / 4.) \
                    - (nuL * S * np.cosh(S / 2.) * np.cosh(nuL) / 2.) \
                    - (2. * nuL**2 * np.cosh(S / 2.) * np.sinh(nuL) / S) \
                    + (2. * nuL**3 * np.cosh(S / 2.) * np.cosh(nuL) / S) \
                    + (2. * nuL * np.sinh(S / 2.) * np.cosh(nuL))

            B = - (nuL * np.cosh(S / 2.) * np.sinh(nuL) * T / (4. * S)) \
                    + (np.cosh(S / 2.) * T * np.cosh(nuL) * (w * self.outer.gamma * kT)**2 / (16. * S)) \
                    - (nuL * np.sinh(S / 2.) * T * np.sinh(nuL) / 8.) \
                    + (np.sinh(S / 2.) * np.cosh(nuL) * w * wd * (self.outer.gamma * kT)**2 / 2.) \
                    + (np.cosh(S / 2.) * T * np.cosh(nuL) * nuL**2 / (4. * S))

            return -B / A

        def gamma(self, omega, nuL, xi, symm = True):
            w = self.outer.w(xi)
            wd = self.outer.wd(xi)

            nuL_dash = self.nuL_dash(omega, nuL, w, symm)

            return 8. * (nuL * nuL_dash * w - nuL**2 * wd) / w**3
