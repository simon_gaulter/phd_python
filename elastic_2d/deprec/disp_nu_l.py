import numpy as np
import scipy.optimize as sciopt
import matplotlib.pyplot as plt
from numpy.lib.scimath import sqrt as csqrt

class Nu(object):

    def __init__(self):
        self.cT = 1.
        self.gamma = .547
        self.h0 = 1.
        self.h1 = 1.5
        self._nuL_range = np.arange(-0.5, 10.01, .01)

    def w(self, xi):
        return 2 * (self.h0 + (self.h1 - self.h0) / np.cosh(xi))

    def wd(self, xi):
        return 2 * (self.h0 - self.h1) * np.tanh(xi) / np.cosh(xi)

class NuL(Nu):
    """
    Various functions to examine the dispersion relation from the
    point of the transverse compressional eigenvalue, nu_L.
    """

    def disp(self, omega, xi, nuL, symm = True):
        """
        Returns the dispersion relation in the form
        D(nuL, xi, nuL) for given omega, xi and nuL.

        Returns a nummpy nd.array.

        Positional arguments:
        omega   -- frequency;
        xi      -- longitudinal coordinate;
        nuL     -- the transverse compressional/longitudinal eigenvalue.

        Keyword arguments:
        symm    -- true/false for symmetric/antisymmetric modes (default True).
        """

        w = self.w(xi)
        kT = omega / self.cT

        ################################################################
        ##  Trigonometric - nuL = i * muL
        ################################################################

        #nuT = csqrt(nuL**2. + (w * kT / 2.)**2. * (1. - self.gamma**2))
        #wf2 = (self.gamma * kT * w / 2.)**2. - nuL**2.

        #if symm:
            #return (wf2 * np.sin(nuT) * np.cos(nuL) \
                    #+ nuT * nuL * np.sin(nuL) * np.cos(nuT)) * 1j
        #else:
            #return (wf2 * np.sin(nuL) * np.cos(nuT) \
                    #+ nuT * nuL * np.sin(nuT) * np.cos(nuL)) * 1j

        ################################################################
        ##  Hyperbolic - nuL \in C
        ################################################################

        nuT = csqrt(nuL**2. + (w * kT / 2.)**2. * (self.gamma**2 - 1.))
        wf2 = nuL**2. + (self.gamma * kT * w / 2.)**2.

        if symm:
            return wf2 * np.sinh(nuT) * np.cosh(nuL) \
                    - nuT * nuL * np.sinh(nuL) * np.cosh(nuT)
        else:
            return wf2 * np.sinh(nuL) * np.cosh(nuT) \
                    - nuL * nuT * np.sinh(nuT) * np.cosh(nuL)

    def disp_plot_complex(self, omega, xi, nuLr_range = None, nuLi_range = None, symm = True):
        """
        Plot the complex components of the nuL roots of the dispersion relation
        D(nuL, xi, nuL), with given xi, omega, using Nick's method.
        
        Positional arguments:
        omega      -- frequency;
        xi         -- longitudinal coordinate.
        
        Keyword arguments:
        nuLr_range -- the range of the real component of nuL;
        nuLi_range -- the range of the imaginary component of nuL;
        symm       -- true/false for symmetric/antisymmetric modes (default True).
        """

        # FIXME - this is producing spurious contours.

        if nuLr_range is None: nuLr_range = self._nuL_range
        if nuLi_range is None: nuLi_range = self._nuL_range

        # Set up a 'complex' open grid for nuL.
        nuL_re = np.expand_dims(nuLr_range, axis = 0)
        nuL_im = np.expand_dims(nuLi_range, axis = 1)
        if nuL_im.dtype is not 'complex128': nuL_im = nuL_im * 1j

        nuL_cx = nuL_re + nuL_im
        self.nuL_cx = nuL_cx

        D = self.disp(omega, xi, nuL_cx, symm)
        self.D = D
        
        plt.clf()
        plt.ion()
        
        ax1 = plt.subplot2grid((2,4), (0,0), colspan = 2)
        ax1.grid('on')
        ax1.set_title(r"$\Re(D(\nu_L)) = 0$")
        ax1.set_xlabel(r"$\Re(\nu_L)$")
        ax1.set_ylabel(r"$\Im(\nu_L)$")
        ax2 = plt.subplot2grid((2,4), (0,2), colspan = 2)
        ax2.grid('on')
        ax2.set_title(r"$\Im(D(\nu_L)) = 0$")
        ax2.set_xlabel(r"$\Re(\nu_L)$")
        ax2.set_ylabel(r"$\Im(\nu_L)$")
        ax3 = plt.subplot2grid((2,4), (1,1), colspan = 2)
        ax3.grid('on')
        ax3.set_title(r"$\Im(D(\nu_L)) = \Re(D(\nu_L)) = 0$")
        ax3.set_xlabel(r"$\Re(\nu_L)$")
        ax3.set_ylabel(r"$\Im(\nu_L)$")

        fig = ax1.get_figure()
        fig.text(0.5, 0.975,\
                r"Dispersion relation $D(\nu_L)$, for sample values "\
                + "$\omega = " + str(omega) + r"$, $\xi = " + str(xi)
                + r"$ and $w(\xi) = 1 + 0.5 \sech(\xi)$",\
                horizontalalignment='center', verticalalignment='top')
        

        lv = [0]
        contour_re = ax1.contour(nuLr_range, nuLi_range, D.real, levels = lv, colors = 'r')
        contour_im = ax2.contour(nuLr_range, nuLi_range, D.imag, levels = lv, colors = 'b')
        contour_cmb_re = ax3.contour(nuLr_range, nuLi_range, D.real, levels = lv, colors = 'r')
        contour_cmb_im = ax3.contour(nuLr_range, nuLi_range, D.imag, levels = lv, colors = 'b')

        plt.show()

    def nuL(self, omega, xi, n = None, symm = True, **kwargs):
        """
        Returns nu_L for a given omega and xi.

        Returns a complex128 if n is specified; numpy ndarray if not.
        Returns None if no roots are found.

        Positional arguments:
        omega   -- frequency;
        xi      -- longitudinal coordinate.

        Keyword arguments:
        n       -- returns only nu_{L,n} if it exists (default None);
        symm    -- true/false for symmetric/antisymmetric modes (default True);
        i_start -- the start point to start searching for nu_L (default 0.0);
        step    -- a finer interval means less chance of missing roots (default 0.1);
        conf_i  -- the iteration at which to ask whether to continue (default 1e4).
        """

        
        i_start = 0.                # The start of the interval to search.
        step = .1                   # The resolution - finer means less chance of missed roots
        conf_i = 1e4                # On the work PC, 6000 ~ 1 second.
        for key in kwargs:
            if key.lower() == "i_start": i_start = kwargs[key]
            if key.lower() == "step": step = kwargs[key]
            if key.lower() == "conf_i": conf_i = kwargs[key]

        disp_rel = lambda nu : self.disp(omega, xi, nu * 1j, symm).imag

        roots = np.array([])

        counter = 0
        cont = True
        while cont:
            try:
                root = sciopt.brentq(disp_rel, i_start, i_start + step)
            except ValueError as e:
                pass
            else:
                roots = np.append(roots, root * 1j)
                if n is not None and roots.size == n:
                    return roots[n - 1]

            i_start += step
            counter += 1
            if counter % conf_i == 0:
                input = raw_input(str(counter) + " iterations completed and " \
                        + str(roots.size) + " mode(s) found.\n" + "Last iteration D(" \
                        + str(i_start) + ") = " + str(disp_rel(i_start)) \
                        + "\nContinue? (y/n): ")
                if input == "y":
                    cont = True
                else:
                    cont = False

        if roots.size > 0 and n is None:
            return roots
        elif roots.size > 0 and n is not None:
            print "Mode " + str(n) + " not found for given xi, omega."
            return None
        else:
            print "No modes found for xi = " + str(xi) + ", omega = " + str(omega) + "."
            return None

    def turning_point(self, omega, n = 1, symm = True):
        """
        Calculate the nth turning point, xi_*, where we have that
        nu_{L,n}(xi_*)^2 + (w(xi_*) * gamma * k_{T,n} / 2.)^2 = 0.
        
        Returns a float if x_* is found, or None otherwise.
        
        Positional arguments:
        omega   -- frequency;
        
        Keyword arguments:
        n       -- Track the nth mode;
        symm    -- true/false for symmetric/antisymmetric modes (default True);
        """

        # TODO - get a better range for xi. We can just use ~700 for now, since
        # np.sinh and np.cosh overflow there anyway...

        # Manual cast to real here to avoid warning.
        tp_rel = lambda xi : ((self.nuL(omega, xi, n, symm)**2.).real \
                + (self.w(xi) * self.gamma * omega / (2. * self.cT))**2.)
        
        step = 0.1
        lbound = 0.
        ubound = lbound + step
        while True:
            try:
                return sciopt.brentq(tp_rel, lbound, ubound)
            except ValueError as e:
                lbound = ubound
                ubound += step
            except TypeError as e:
                print "No turning point found."
                return None
