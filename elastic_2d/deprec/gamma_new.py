import numpy as np
import scipy.optimize as sciopt
import matplotlib.pyplot as plt
from numpy.lib.scimath import sqrt as csqrt

class Gamma(object):

    def __init__(self):
        self.nuL = self.NuL(self)
        self.nuT = self.NuT(self) 
        self._omega = np.arange(1, 4+.01, .01)
        self._cT = 1.
        self._gamma = .547
        self._h0 = 1.
        self._h1 = 1.5

    @property
    def omega(self):
        return self._omega

    @omega.setter
    def omega(self, value):
        self._omega = value

    @property
    def cT(self):
        return self._cT

    @cT.setter
    def cT(self, value):
        self._cT = value

    @property
    def gamma(self):
        return self._gamma

    @gamma.setter
    def gamma(self, value):
        self._gamma = value

    @property
    def h0(self):
        return self._h0

    @h0.setter
    def h0(self, value):
        self._h0 = value

    @property
    def h1(self):
        return self._h1

    @h1.setter
    def h1(self, value):
        self._h1 = value

    def w(self, xi):
        return 2 * (self.h0 + (self.h1 - self.h0) / np.cosh(xi))

    def wd(self, xi):
        return 2 * (self.h0 - self.h1) * np.tanh(xi) / np.cosh(xi)

    def _disp_rel_plot(self, omega, bNu_range, xi, disp_fn, symm = True):
        """Utility function to plot the dispersion relation,
        D, for a SINGLE omega and range of bNuL or bNuT.

        bNuL or bNuT are the zeroes of this plot.

        This should not be called directly, but from the inner
        classes NuL, NuT. """
        disp_rel = disp_fn(omega, bNu_range, self.w(xi), symm)
        plt.ion()
        plt.grid(True)
        plt.plot(np.expand_dims(bNu_range, axis=1), disp_rel) 
        plt.show()

    class NuL(object):

        def __init__(self, outer):
            self.outer = outer
            self._nuL_range = np.arange(0+.1, 10+.1, .1)

        @property
        def nuL_range(self):
            return self._nuL_range

        @nuL_range.setter
        def nuL_range(self, value):
            self._nuL_range = value

        def turning_point(self, omega, nuL_range, xi_range, n, symm = True):
            """Returns xi_*, nuL_n(xi_*) for the turning point, xi_*, where 
            nuL_n(xi_*)^2 + (w(xi_*) * k_L / 2)^2 = 0."""

            kL = self.outer.gamma * omega / self.outer.cT

            tp_rel = lambda xi : (self.nuL(omega, nuL_range, xi, symm)[n])**2. \
                    + (self.outer.w(xi) * kL / 2.)**2.
            try:
                xi_s = sciopt.brentq(tp_rel, xi_range[0], xi_range[1])
                return xi_s, self.nuL(omega, nuL_range, xi_s, symm)[n]
            except ValueError as e:
                print "No turning point found in the given xi_range."
                return None
            except IndexError as e:
                print "No nu_(L," + str(n) + ") found in the given nuL_range."
                return None

        def disp_rel(self, omega, nuL_range, xi, symm = True):
            """Calculate dispersion relation, D, for a given range
            of bNuL in iR, omega in R and single xi.

            Assumes nuL = i*bNuL for nuL in iR.
            Use broadcasting to calculate over a range.

            This method is passed to the outer class's methods to calculate
            roots or plot bNu."""

            w = self.outer.w(xi)

            omega = np.expand_dims(omega, axis = 0)

            nuL = np.expand_dims(nuL_range, axis = 1)

            kT = omega / self.outer.cT
            nuT = csqrt(nuL**2 + (w * kT)**2 * (self.outer.gamma**2 - 1.) / 4.)
            wf2 = nuL**2 + (self.outer.gamma * kT * w)**2 / 4.

            if symm:
                return wf2 * np.sinh(nuT) * np.cosh(nuL) \
                        - nuT * nuL * np.sinh(nuL) * np.cosh(nuT)
            else:
                return wf2 * np.sinh(nuL) * np.cosh(nuT) \
                        - nuL * nuT * np.sinh(nuT) * np.cosh(nuL)

        def nuL(self, omega, nuL_range, xi, symm = True):
            """Find multiple nuL in R and iR as a root of the dispersion
            relation for a given range in R and iR."""

            roots_re = np.array([])
            roots_im = np.array([])

            # This will find the point where disp_rel switches from R to iR only.
            disp_re_sw = lambda nu : (self.disp_rel(omega, nu, xi, symm)**2).real
            disp_im_sw = lambda nu : \
                    (self.disp_rel(omega, nu * 1j, xi, symm)**2).real

            try:
                root_re_sw = sciopt.brentq(disp_re_sw, nuL_range[0],\
                        nuL_range[nuL_range.size - 1], xtol=1e-16)
            except ValueError as e:
                print "Dispersion relation does not change from R to iR for \
                        real nu."
            else:
                print "root_re_sw:", root_re_sw
                roots_re = np.append(roots_re, root_re_sw)

            try:
                root_im_sw = sciopt.brentq(disp_im_sw, nuL_range[0],\
                        nuL_range[nuL_range.size - 1], xtol=1e-16)
            except ValueError as e:
                print "Dispersion relation does not change from R to iR for \
                        imaginary nu."
            else:
                print "root_im_sw:", root_im_sw
                roots_im = np.append(roots_im, root_im_sw * 1j)


            # This will find the other zeros.
            disp_re = lambda nu : (self.disp_rel(omega, nu, xi, symm)\
                    [self.disp_rel(omega, nu, xi, symm).imag != 0]).imag
            disp_im = lambda nu : (self.disp_rel(omega, nu * 1j, xi, symm)\
                    [self.disp_rel(omega, nu * 1j, xi, symm).imag != 0]).imag

            for i in range(nuL_range.size - 1):
                try:
                    root_re = sciopt.brentq(disp_re, nuL_range[i], nuL_range[i + 1])
                except ValueError as e:
                    pass
                except TypeError as e:
                    pass
                else:
                    roots_re = np.append(roots_re, root_re)

                try:
                    root_im = sciopt.brentq(disp_im, nuL_range[i], nuL_range[i + 1])
                except ValueError as e:
                    pass
                except TypeError as e:
                    pass
                else:
                    roots_im = np.append(roots_im, root_im * 1j)

            return np.append(roots_re, roots_im)

        def nuL_dash(self, omega, nuL, xi, symm = True):
            """Calculate nuL' for a SINGLE omega and nuL."""

            kT = omega / self.outer.cT

            w = self.outer.w(xi)
            wd = self.outer.wd(xi)

            S = csqrt((self.outer.gamma**2 - 1) * (w * kT)**2 + 4. * nuL**2)
            T = 2. * (self.outer.gamma**2 - 1) * w * wd * kT**2

            A = - (S * np.cosh(S / 2.) * np.sinh(nuL) / 2.) \
                    + (nuL * np.cosh(S / 2.) * np.cosh(nuL) \
                    * (w * self.outer.gamma * kT)**2 / (2. * S)) \
                    + (np.sinh(S / 2.) * np.sinh(nuL) \
                    * (w * self.outer.gamma * kT)**2 / 4.) \
                    - (nuL * S * np.cosh(S / 2.) * np.cosh(nuL) / 2.) \
                    - (2. * nuL**2 * np.cosh(S / 2.) * np.sinh(nuL) / S) \
                    + (2. * nuL**3 * np.cosh(S / 2.) * np.cosh(nuL) / S) \
                    + (2. * nuL * np.sinh(S / 2.) * np.cosh(nuL))

            B = - (nuL * np.cosh(S / 2.) * np.sinh(nuL) * T / (4. * S)) \
                    + (np.cosh(S / 2.) * T * np.cosh(nuL) \
                    * (w * self.outer.gamma * kT)**2 / (16. * S)) \
                    - (nuL * np.sinh(S / 2.) * T * np.sinh(nuL) / 8.) \
                    + (np.sinh(S / 2.) * np.cosh(nuL) * w * wd \
                    * (self.outer.gamma * kT)**2 / 2.) \
                    + (np.cosh(S / 2.) * T * np.cosh(nuL) * nuL**2 / (4. * S))

            return -B / A

        def gamma(self, omega, nuL, xi, symm = True):
            w = self.outer.w(xi)
            wd = self.outer.wd(xi)

            nuL_dash = self.nuL_dash(omega, nuL, w, symm)

            return 8. * (nuL * nuL_dash * w - nuL**2 * wd) / w**3

    class NuT(object):

        def __init__(self, outer):
            self.outer = outer
            self._nuT_range = np.arange(0+.1, 10+.1, .1)

        @property
        def nuT_range(self):
            return self._nuT_range

        @nuT_range.setter
        def nuT_range(self, value):
            self._nuT_range = value
