import numpy as np
import scipy.optimize as sciopt
import matplotlib.pyplot as plt
from numpy.lib.scimath import sqrt as csqrt

class Gamma:

    step = .01
    omega_range = np.expand_dims(np.arange(1, 4+step, step), axis=0)
    nuL_range = np.expand_dims(np.arange(0, 2+step, step), axis=1)
    cT = 1.
    gamma = .547

    # We're assuming that nuL, nuT are in iR, so
    # look for nuL = i*bNul; nuT = i*bNuT instead.
    def disp_rel(self, omega, bNuL, w):
        cT = self.cT
        gamma = self.gamma
        
        kT = omega / cT
        bNuT = csqrt(bNuL**2 + w**2 * (1 - gamma**2) * kT**2 / 4.)
        wf2 = (gamma * kT * w)**2 / 4. - bNuL**2

        return wf2 * np.sin(bNuT) * np.cos(bNuL) \
                + bNuT * bNuL * np.sin(bNuL) * np.cos(bNuT)

    # nuL = i*barNuL
    def barNuL(self, omega, w):
        cT = self.cT
        gamma = self.gamma
        nu = np.empty(omega.shape)
        nu[:] = np.nan
        nuL_range = self.nuL_range

        for idx, o in enumerate(omega[0]):

            self.zS = lambda n : self.disp_rel(o, n, w)

            #self.zS = lambda nuL: ((nuL**2 + (w * gamma * kT / 2.)**2) \
                    #* np.sinh(csqrt(nuL**2 + (gamma**2 - 1.) * (w * kT / 2.)**2)) \
                    #* np.cosh(nuL) - nuL * csqrt(nuL**2 + (gamma**2 - 1.) \
                    #* (w * kT / 2.)**2) * np.sinh(nuL) * np.cosh(csqrt(nuL**2 \
                    #+ (gamma**2 - 1.) * (w * kT / 2.)**2)))**2
            try:
                nu[0,idx] = sciopt.brentq(self.zS, nuL_range[0,0], nuL_range[nuL_range[:,0].size - 1,0])
            except ValueError:
                print idx, o

        return nu

    def plot_nuL(self, omega, nuL, w):

        ax = plt.subplot(1,1,1)

        cT = self.cT
        gamma = self.gamma
        kT = omega / cT

        zS = self.disp_rel(omega, nuL, w)
        cS = ax.contour(omega[0], nuL[:,0], zS, [0], antialiased=True, colors='g')
        #zSr = zS.copy()
        #zSr[abs(zSr.imag) > 1e-8] = np.nan+np.nan*1j

        #zSi = zS.copy()
        #zSi[abs(zSi.real) > 1e-8] = np.nan+np.nan*1j
        
        #cSi = ax.contour(omega[0], nuL[:,0], zSi.imag, [0], antialiased=True, colors='b')
        #cSr = ax.contour(omega[0], nuL[:,0], zSr.real, [0], antialiased=True, colors='r')
        plt.grid()
        plt.show()

    def nuL_dash(self, omega, w, wd):
        cT = self.cT
        gamma = self.gamma
        nuL = self.barNuL(omega, w) * 1j

        kT = omega / cT

        S = csqrt(w**2 * gamma**2 * kT**2 - w**2 * kT**2 + 4 * nuL**2)
        T = w * gamma**2 * kT**2 * wd - w * kT**2 * wd

        A = - (S * np.cosh(S / 2.) * np.sinh(nuL) / 2.) \
                + (nuL * np.cosh(S / 2.) * np.cosh(nuL) * w**2 * gamma**2 * kT**2 \
                / (2. * S)) + (np.sinh(S / 2.) * np.sinh(nuL) * w**2 * gamma**2 \
                * kT**2 / 4.) - (nuL * S * np.cosh(S / 2.) * np.cosh(nuL) / 2.) \
                - (2. * nuL**2 * np.cosh(S / 2.) * np.sinh(nuL) / S) \
                + (2. * nuL**3 * np.cosh(S / 2.) * np.cosh(nuL) / S) \
                + (2. * np.sinh(S / 2.) * np.cosh(nuL) * nuL)

        B = - (nuL * np.cosh(S / 2.) * np.sinh(nuL) * T / (2. * S)) \
                + (np.cosh(S / 2.) * T * np.cosh(nuL) * w**2 * gamma**2 * kT**2 \
                / (8. * S)) - (nuL * np.sinh(S / 2.) * T * np.sinh(nuL) / 4.) \
                + (np.sinh(S / 2.) * np.cosh(nuL) * w * gamma**2 * kT**2 \
                * wd / 2.) + (np.cosh(S / 2.) * T * np.cosh(nuL) * nuL**2 / (2. * S))

        return A / B

    def gam(self, omega, w, wd):
        nuL = self.barNuL(omega, w) *1j
        nuL_dash = self.nuL_dash(omega, w, wd)

        return np.real(8. * (nuL * nuL_dash * w - nuL**2 * wd) / w**3)
