# -*- coding: utf-8 -*-
"""
Created Tuesday 7th August 2012

@author: Simon Gaulter
"""

import numpy as np
import matplotlib.pyplot as plt
from numpy.lib.scimath import sqrt as csqrt

class F_dash:
    """ Calculate and plot the dispersion curves for the 2D elastic plate.
    Default values correspond to those for a steel plate of constant width w =2.
    """

    def __init__(self):
        self.omega_range = np.arange(1., 10.01, .01)
        self.f_dash_range = np.arange(0., 10.01, .01)
        self.cT = 1.
        self.gamma = .547
        self.h0 = 1.
        self.h1 = 1.5
    
    def w(self, xi):
        return 2. * (self.h0 + (self.h1 - self.h0) / np.cosh(xi))

    def wd(self, xi):
        return 2. * (self.h0 - self.h1) * np.tanh(xi) / np.cosh(xi)

    def disp(self, xi, omega, f_dash, symm = True):
        """ Calculate the dispersion curves implicitly. """

        w = self.w(xi)
        #omega = self.omega
        #f_dash = self.f_dash
        cT = self.cT
        gamma = self.gamma

        # Propagating
        nuL = w/2. * csqrt( f_dash**2. - ( gamma * omega / cT )**2. )
        nuT = w/2. * csqrt( f_dash**2. - ( omega / cT )**2. )
        
        if symm:
            return (w/2.)**2. * f_dash**2. * np.sinh(nuT) * np.cosh(nuL) \
                - nuL * nuT * np.sinh(nuL) * np.cosh(nuT)
        else:
            return (w/2.)**2 * f_dash**2 * np.cosh(nuT) * np.sinh(nuL) \
                - nuL * nuT * np.sinh(nuT) * np.cosh(nuL)

        #if symm:
            #return (w/2.)**2. * f_dash**2. * (-1j) * np.sin(1j * nuT) * np.cos(1j * nuL) \
                #- nuL * nuT * (-1j) * np.sin(1j * nuL) * np.cos(1j * nuT)
        #else:
            #return (w/2.)**2 * f_dash**2 * np.cos(1j * nuT) * (-1j) * np.sin(1j * nuL) \
                #- nuL * nuT * (-1j) * np.sin(1j * nuT) * np.cos(1j * nuL)

    def curves(self, xi, omega_range = None, f_dash_range = None, symm = True, **kwargs):
        """ Plot the dispersion curves as the zero contours of dispersion(). """

        save_fig = False
        bound_lines = False
        f_imag = False
        invert_y = False
        for key in kwargs:
            if key.lower() == "save_fig": save_fig = kwargs[key]
            if key.lower() == "bound_lines": bound_lines = kwargs[key]
            if key.lower() == "f_imag": f_imag = kwargs[key]
            if key.lower() == "invert_y": invert_y = kwargs[key]

        if omega_range is None: omega_range = self.omega_range
        if f_dash_range is None: f_dash_range = self.f_dash_range

        omega_2d = np.expand_dims(omega_range, axis = 0)
        f_dash_2d = np.expand_dims(f_dash_range, axis = 1)

        if f_imag: f_dash_2d = f_dash_2d * (-1j)

        if invert_y:
            f_dash_2d = omega_2d / f_dash_2d

        zA = self.disp(xi, omega_2d, f_dash_2d, symm = False)
        zS = self.disp(xi, omega_2d, f_dash_2d, symm = True)

        plt.clf()
        plt.ion()
        ax = plt.subplot(111)
        plt.rcParams['mathtext.default'] = 'regular'

        rnd_threshold = 1e-11

        # Antisymmetric - imaginary part of equation.
        zAi = zA.copy()
        zAi[abs(zAi.real) > rnd_threshold] = np.nan + np.nan * 1j
        cAi = ax.contour(omega_range, f_dash_range, zAi.imag, [0], colors='r',\
                antialiased = True, linestyles = '-')
        cAi.collections[0].set_label('antisymm')

        # Symmetric - imaginary part of equation.
        zSi = zS.copy()
        zSi[abs(zSi.real) > rnd_threshold] = np.nan + np.nan * 1j
        cSi = ax.contour(omega_range, f_dash_range, zSi.imag, [0], colors='b',\
                antialiased = True, linestyles = '-')
        cSi.collections[0].set_label('symm')

        # Antisymmetric - real part of equation.
        zAr = zA.copy()
        zAr[abs(zAr.imag) > rnd_threshold] = np.nan + np.nan * 1j
        cAr = ax.contour(omega_range, f_dash_range, zAr.real, [0], colors='r',\
                antialiased = True, linestyles = '-')

        # Symmetric - real part of equation.
        zSr = zS.copy()
        zSr[abs(zSr.imag) > rnd_threshold] = np.nan+np.nan*1j
        cSr = ax.contour(omega_range, f_dash_range, zSr.real, [0], colors='b',\
                antialiased = True, linestyles = '-')

        if bound_lines:
            ax.plot(omega_range, omega_range / self.cT, 'k:',
                    label = r"$f' = \frac{\omega}{cT}$")
            ax.plot(omega_range, self.gamma * omega_range / self.cT, 'k--',
                    label = r"$f' = \frac{\gamma\omega}{cT}$")

        plt.legend(loc='upper left')

        plt.xlabel(r"$\omega$", fontsize = 14)
        if invert_y:
            ylab = "c"
        else:
            ylab = "f'"
        if f_imag:
            plt.title("Dispersion curves for $\gamma = " + str(self.gamma) \
                    + "$ and imaginary $" + ylab + "$.")
            plt.ylabel(r"$" + ylab + " \in i\mathbb{R}$", fontsize = 14)
        else:
            plt.title("Dispersion curves for $\gamma = " + str(self.gamma) \
                    + "$ and real $" + ylab + "$.")
            plt.ylabel(r"$" + ylab + "$", fontsize = 14)
        plt.grid()
        plt.show()

        if save_fig:
            plt.savefig('dispersion')
