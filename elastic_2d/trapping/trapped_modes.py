# -*- coding: utf-8 -*-

import scipy as sp
import scipy.optimize as spo
import scipy.special as spc
import scipy.integrate as spi
import matplotlib.pyplot as pl
pl.ion()
import time
import datetime

class Trapping:

	EQUIVOLUMINAL = "equivoluminal"
	DILATIONAL = "dilational"
	SYMMETRIC = "symmetric"
	ANTISYMMETRIC = "antisymmetric"

	def __init__(self):
		self.set_params(eps=0.1,gamma=0.547,h1=1.5,p=1,\
				longsymmetry='symmetric',transsymmetry=self.ANTISYMMETRIC,displacement=self.EQUIVOLUMINAL)

	def get_params(self):
		"""Return the current parameters."""
		print "eps:            " + str(self.eps)
		print "h1:             " + str(self.h1)
		print "longsymmetry:   " + str(self.longsymmetry)
		print "transsymmetry:  " + str(self.transsymmetry)
		print "displacement:   " + str(self.displacement)
		print "p:              " + str(self.p)

	def set_params(self,eps=None,gamma=None,h1=None,p=None,\
			longsymmetry=None,transsymmetry=None,displacement=None):
		"""Optionally change the parameters.

		Keyword arguments:
			eps           -- one or more values of epsilon to use;
			h1            -- one or more values of h1 to use;
			NOTE: Only one of the above may have dimension > 0 at a time
			p             -- longitudinal mode;
			displacement  -- EQUIVOLUMINAL or
			                 DILATIONAL;
			symmetry      -- ANTISYMMETRIC (flexural) phi antisymmetric / psi symmetric, or 
			                 SYMMETRIC (longitudinal) phi symmetric / psi antisymmetric;"""
		if gamma is not None:
			self.gamma = gamma
		if transsymmetry is not None:
			self.transsymmetry = transsymmetry
		if longsymmetry is not None:
			self.longsymmetry = longsymmetry
		if displacement is not None:
			self.displacement = displacement
		if p is not None:
			self.p = p
		if ((isinstance(eps,sp.ndarray) and eps.size > 1) \
				and (isinstance(h1,sp.ndarray) and h1.size > 1)):
			print "Error: We can iterate over a range of EITHER h1 or epsilon."
			return
		if h1 is not None:
			# h1 max bulge height
			if isinstance(h1,sp.ndarray):
				self.h1 = h1
			else:
				self.h1 = sp.array([h1])
		if eps is not None:
			# 0 < eps << 1
			if isinstance(eps,sp.ndarray):
				self.eps = eps
			else:
				self.eps = sp.array([eps])

		# Get the pth root of either Ai (antisymmetric) or Ai' (symmetric)
		if self.longsymmetry == self.SYMMETRIC:
			self.aiz = spc.ai_zeros(self.p)[1][-1]
		else:
			self.aiz = spc.ai_zeros(self.p)[0][-1]

		# Refresh all output once parameters are changed.
		if hasattr(self,'k'): del self.k
		if hasattr(self,'lastcalled'): del self.lastcalled

	def hm(self,x):
		return 1 + (self.h1 - 1.) * sp.exp(-(x)**2.)

	def hp(self,x):
		return 1 + (self.h1 - 1.) * sp.exp(-(x)**2.)

	def w(self,x):
		return self.hm(x) + self.hp(x)

	def disp_rel_kwt(self,k,tau):
		# Calculate the ξ-independent dispersion relation
		# between w κ_ψ / 4 and τ_ψ
		gamma = self.gamma
		if self.displacement == self.EQUIVOLUMINAL:
			tpsi = tau
			tphi = sp.sqrt(tau**2. + (gamma**2. - 1.) * k**2.)
		elif self.displacement == self.DILATIONAL:
			tphi = tau
			tpsi = sp.sqrt(tau**2. + (1. - gamma**2.) * k**2.)
		
		if self.transsymmetry == self.ANTISYMMETRIC:
			return (2. * tpsi**2. -  k**2.)**2. * sp.sin(tphi) * sp.cos(tpsi) - \
					(4. * tpsi**2. - 4. * k**2.) * tphi * tpsi * sp.sin(tpsi) * sp.cos(tphi)
		elif self.transsymmetry == self.SYMMETRIC:
			return (2. * tpsi**2. -  k**2.)**2. * sp.cos(tphi) * sp.sin(tpsi) - \
					(4. * tpsi**2. - 4. * k**2.) * tphi * tpsi * sp.cos(tpsi) * sp.sin(tphi)

	def disp_rel_kt(self,x,k,tau):
		# Calculate the dispersion relation
		# between κ_ψ and τ_ψ
		w = self.w(x)
		gamma = self.gamma
		if self.displacement == self.EQUIVOLUMINAL:
			tpsi = tau
			tphi = sp.sqrt(tau**2. + (w / 2.)**2. * (gamma**2. - 1.) * k**2.)
		elif self.displacement == self.DILATIONAL:
			tphi = tau
			tpsi = sp.sqrt(tau**2. + (w / 2.)**2. * (1. - gamma**2.) * k**2.)
		
		if self.transsymmetry == self.ANTISYMMETRIC:
			return (2. * tpsi**2. - (w / 2.)**2. * k**2.)**2. * sp.sin(tphi) * sp.cos(tpsi) - \
					(4. * tpsi**2. - w**2. * k**2.) * tphi * tpsi * sp.sin(tpsi) * sp.cos(tphi)
		elif self.transsymmetry == self.SYMMETRIC:
			return (2. * tpsi**2. - (w / 2.)**2. * k**2.)**2. * sp.cos(tphi) * sp.sin(tpsi) - \
					(4. * tpsi**2. - w**2. * k**2.) * tphi * tpsi * sp.cos(tpsi) * sp.sin(tphi)

	def disp_rel_kg(self,x,k,g):
		# Calculate the dispersion relation
		# between κ_ψ and g g'
		w = self.w(x)
		gamma = self.gamma
		tphi = w / 2. * sp.sqrt(-g + gamma**2. * k**2.)
		tpsi = w / 2. * sp.sqrt(-g + k**2.)
		if self.transsymmetry == self.ANTISYMMETRIC:
			return (w / 2.)**2. * (2. * -g + k**2.)**2. * sp.sin(tphi) * sp.cos(tpsi) - \
					4. * -g * tphi * tpsi * sp.cos(tphi) * sp.sin(tpsi)
		elif self.transsymmetry == self.SYMMETRIC:
			return (w / 2.)**2. * (2. * -g + k**2.)**2. * sp.cos(tphi) * sp.sin(tpsi) - \
					4. * -g * tphi * tpsi * sp.sin(tphi) * sp.cos(tpsi)

	def curves_xt(self,x_range,t_range,k,**kwargs):
		xx,ttau = self.__to_grid(x_range,t_range)
		col = 'r' if self.transsymmetry == self.ANTISYMMETRIC else 'b'
		disp = self.__to_real(self.disp_rel_kt(xx,k,ttau))
		con = pl.contour(xx,ttau,disp,[0],colors=col,**kwargs)
		con.collections[0].set_label(self.transsymmetry.title())
		pl.title(r'$\tau_{\psi}(\xi, \kappa_{\psi})$ against $\xi$ for $\kappa_{\psi} = %.1f$' % k)
		pl.xlabel(r'$\xi$')
		pl.ylabel(r'$\tau_{\psi}$')
		pl.grid(True)
		pl.ion()
		pl.show()
		pl.legend()
		return con

	def curves_kt(self,k_range,tau_range,x=None,roots=True,**kwargs):
		kk,ttau = self.__to_grid(k_range,tau_range)
		col = 'r' if self.transsymmetry == self.ANTISYMMETRIC else 'b'
		if x is None:
			disp = self.__to_real(self.disp_rel_kwt(kk,ttau))
			xlab = r'$\displaystyle{\frac{w \kappa_{\psi}}{2}}$'
		else:
			disp = self.__to_real(self.disp_rel_kt(x,kk,ttau))
			xlab = r'$\kappa_{\psi}$'
		if len(pl.gcf().get_axes()) > 0:
			ax1 = pl.gcf().get_axes()[0]
		else:
			ax1 = pl.subplot(111)
		con = ax1.contour(kk,ttau,disp,[0],colors=col,**kwargs)
		con.collections[0].set_label(self.transsymmetry.title())
		pl.rc('text', usetex=True)
		pl.rc('font', size=14)
		ax1.set_xlabel(xlab)
		ylabel = r'$\tau_{\psi}$' if self.displacement == self.EQUIVOLUMINAL else r'$\tau_{\phi}$'
		ax1.set_ylabel(ylabel)
		ax1.grid(True)
		pl.ion()
		pl.show()
		ax1.legend(loc='upper left')

#{{{
		if roots and x is None:
			k = sp.linspace(max(min(k_range),min(tau_range)),min(max(k_range),max(tau_range)))
			if self.displacement == self.EQUIVOLUMINAL:
				pl.plot(k,k,ls='--',c='#555555')
			else:
				pl.plot(k,self.gamma * k,ls='--',c='#555555')

			if len(pl.gcf().get_axes()) > 1:
				ax2 = pl.gcf().get_axes()[1]
				xticks = ax2.get_xticks().tolist()
				xticklabs = [xl.get_text() for xl in ax2.get_xticklabels()]
			else:
				ax2 = pl.twiny()
				ax2.set_xlim(ax1.get_xlim())
				xticks = sp.array([])
				xticklabs = sp.array([])

			n = 1
			while True:
				if self.transsymmetry == self.ANTISYMMETRIC:
					r = (2. * n - 1.) * sp.pi / 2.
					m = str(2 * n - 1) if n > 1 else ''
					rlab = r'$\displaystyle{\frac{%s \pi}{2}}$' % m
				else:
					r = n * sp.pi
					m = str(n) if n > 1 else ''
					rlab = r'$%s \pi$' % m
				if r > max(k_range):
					break
				xticks = sp.append(xticks,r)
				xticklabs = sp.append(xticklabs,rlab)
				pl.axvline(r,ls='--',c=col,alpha=0.4)
				n = n + 1

			n = 1
			while True:
				if self.transsymmetry == self.ANTISYMMETRIC:
					r = n * sp.pi / self.gamma
					m = str(n) if n > 1 else ''
					rlab = r'$\displaystyle{\frac{%s \pi}{\gamma}}$' % m
				else:
					r = (2. * n - 1.) * sp.pi / (2. * self.gamma)
					m = str(2 * n - 1) if n > 1 else ''
					rlab = r'$\displaystyle{\frac{%s \pi}{2 \gamma}}$' % m
				if r > max(k_range):
					break
				xticks = sp.append(xticks,r)
				xticklabs = sp.append(xticklabs,rlab)
				pl.axvline(r,ls='--',c=col,alpha=0.4)
				n = n + 1

			ax2.set_xticks(xticks)
			ax2.set_xticklabels(xticklabs)

		elif roots and x is not None:
			k = sp.linspace(max(min(k_range),min(tau_range)),min(max(k_range),max(tau_range)))
			if self.displacement == self.EQUIVOLUMINAL:
				pl.plot(k,self.w(x) / 2. * k,ls='--',c='#555555')
			else:
				pl.plot(k,self.gamma * self.w(x) / 2. * k,ls='--',c='#555555')

			if len(pl.gcf().get_axes()) > 1:
				ax2 = pl.gcf().get_axes()[1]
				xticks = ax2.get_xticks().tolist()
				xticklabs = [xl.get_text() for xl in ax2.get_xticklabels()]
			else:
				ax2 = pl.twiny()
				ax2.set_xlim(ax1.get_xlim())
				xticks = sp.array([])
				xticklabs = sp.array([])

			n = 1
			while True:
				if self.transsymmetry == self.ANTISYMMETRIC:
					r = (2. * n - 1.) * sp.pi / self.w(x)
					m = str(2 * n - 1) if n > 1 else ''
					rlab = r'$\displaystyle{\frac{%s \pi}{w(\xi)}}$' % m
				else:
					r = 2. * n * sp.pi / self.w(x)
					rlab = r'$\displaystyle{\frac{%s \pi}{w(\xi)}}$' % (2*n)
				if r > max(k_range):
					break
				xticks = sp.append(xticks,r)
				xticklabs = sp.append(xticklabs,rlab)
				pl.axvline(r,ls='--',c=col,alpha=0.4)
				n = n + 1

			n = 1
			while True:
				if self.transsymmetry == self.ANTISYMMETRIC:
					r = 2. * n * sp.pi / (self.w(x) * self.gamma)
					rlab = r'$\displaystyle{\frac{%s \pi}{\gamma w(\xi)}}$' % (2*n)
				else:
					r = (2. * n - 1.) * sp.pi / (self.w(x) * self.gamma)
					m = str(2 * n - 1) if n > 1 else ''
					rlab = r'$\displaystyle{\frac{%s \pi}{\gamma w(\xi)}}$' % m
				if r > max(k_range):
					break
				xticks = sp.append(xticks,r)
				xticklabs = sp.append(xticklabs,rlab)
				pl.axvline(r,ls='--',c=col,alpha=0.4)
				n = n + 1

			ax2.set_xticks(xticks)
			ax2.set_xticklabels(xticklabs)
#}}}
		return con

	def curves_kg(self,k_range,g_range,x,**kwargs):
		kk,gg = self.__to_grid(k_range,g_range)
		col = 'r' if self.transsymmetry == self.ANTISYMMETRIC else 'b'
		#FIXME - g**2 is a bit of a bodge, just to get an idea of the shape
		disp = self.__to_real(self.disp_rel_kg(x,kk,gg))
		con = pl.contour(kk,gg,disp,[0],colors=col,**kwargs)
		con.collections[0].set_label(self.transsymmetry.title())
		pl.title(r"$- g g'^2$ against $\kappa_{\psi}$ for $\xi = %.1f$" % x)
		pl.xlabel(r'$\kappa_{\psi}$')
		pl.ylabel(r"$- g g'^2$")
		pl.grid(True)
		pl.ion()
		pl.show()
		pl.legend()
		return con

	def curves_xg(self,x_range,g_range,k,**kwargs):
		xx,gg = self.__to_grid(x_range,g_range)
		col = 'r' if self.transsymmetry == self.ANTISYMMETRIC else 'b'
		disp = self.__to_real(self.disp_rel_kg(xx,k,gg))
		con = pl.contour(xx,gg,disp,[0],colors=col,**kwargs)
		con.collections[0].set_label(self.transsymmetry.title())
		pl.title(r"$- g g'^2$ against $\xi$ for $\kappa_{\psi} = %.1f$" % k)
		pl.xlabel(r'$\xi$')
		pl.ylabel(r"$- g g'^2$")
		pl.grid(True)
		pl.ion()
		pl.show()
		pl.legend()
		return con

	def bisection(self, f, xmin, xmax, grain=500):
		xlin = sp.linspace(xmin, xmax, grain)
		roots = []
		for i in range(0,len(xlin)-1):
			try:
				x,r = spo.bisect(f, xlin[i], xlin[i+1], full_output=True)
				if r.converged:
					roots.append(x)
			except ValueError:
				pass
		return sp.asarray(roots)

	def tau(self,x,k,mode=None):
		dr = lambda tau: self.__to_real(self.disp_rel_kt(x,k,tau))
		# slow but sure
		if mode is not None:
			r = self.bisection(dr,0,self.cut_on(mode)*3,grain=20)
		else:
			r = self.bisection(dr,0,k**2.,grain=500)
		
		# eliminate spurious zeros
		TZERO = 1.0e-4 # tolerance for 'zero'
		r = r[abs(dr(r)) < TZERO]
		r = r[r > 0]
		if len(r) == 0:
			return None

		TOL = 1.0e-4 # tolerance for 'equality'
		r.sort()
		if mode is not None:
			try:
				return r[mode-1]
			except IndexError:
				return sp.nan
		else:
			prop = r < self.w(x) * k / 2
			return r[sp.hstack([1, sp.diff(r)]) > TOL], prop

	def tau_n(self,x,k,n):
		x = sp.asarray(x)
		k = sp.asarray(k)
		if x.ndim == 0: x = sp.expand_dims(x,0)
		if k.ndim == 0: k = sp.expand_dims(k,0)
		try:
			it = sp.nditer([x,k,None],op_axes=[[0,-1],[-1,0],None])
		except ValueError:
			return self.tau(x,k,mode=n)
		for xx, kk, y in it:
			y[...] = self.tau(xx,kk,mode=n)
		return it.operands[2].T

	def tau_grid(self,x,k,n):
		tau = sp.full((len(k),len(x)),sp.nan)
		tstart = time.time()
		for xi in range(0,len(x)):
			for ki in range(0,len(k)):
				itstart = time.time()
				try:
					r = self.tau(x[xi],k[ki])[0][n-1]
					if r is not None: tau[ki,xi] = r
				except IndexError:
					pass
				except TypeError:
					pass
				if xi == 0 and ki == 0:
					print "Started at {0}".format(time.ctime())
					print "Projected time for {0} loops: {1}".format(len(x) * len(k),datetime.timedelta(seconds=round((time.time()-itstart) * len(k) * len(x))))
		print "Elapsed time: {0}".format(datetime.timedelta(seconds=round((time.time()-tstart))))
		return tau

	def cut_on(self,mode):
		"""Return the cut-on frequencies for a given mode."""
		if self.transsymmetry == self.SYMMETRIC:
			r1 = [ n * sp.pi for n in range(1, mode + 2)]
			r2 = [ (2 * n - 1) * sp.pi / (2 * self.gamma) for n in range(1, mode + 2)]
		else:
			r1 = [ (2 * n - 1) * sp.pi / 2. for n in range(1, mode + 2)]
			r2 = [ n * sp.pi / (self.gamma) for n in range(1, mode + 2)]
		return sp.sort(sp.hstack((r1,r2)))[mode-1]

	def xstar(self,k,mode,**kwargs):
		"""Return the turning point, ξ*, for a given k and mode number."""
		root = self.cut_on(mode)
		return abs(spo.root(lambda x: 2. * root / k - self.w(x),0,method='broyden1',**kwargs)['x'])

	def g(self,x,k,mode):
		"""Return the phase function, g(x,k). Takes only positive values of x."""
		it = sp.nditer([x,None])
		for xx,y in it:
			if sp.isnan(self.tau_n(xx,k,mode)):
				y[...] = sp.nan
			if 2. / self.w(xx) * self.tau_n(xx,k,mode) < k:
			#if xx < self.xstar(k,mode):
				# propagating
				y[...] = -((3./2 * spi.quad(\
						lambda barx: sp.real(sp.sqrt(k**2 - 4. / self.w(barx)**2. * self.tau_n(barx,k,mode)**2)),\
						xx, self.xstar(k,mode))[0])**2.)**(1./3)
				#print xx, 'propagating'
			else:
				y[...] = ((3./2 * spi.quad(\
						lambda barx: sp.real(sp.sqrt(4./ self.w(barx)**2. * self.tau_n(barx,k,mode)**2 - k**2)),\
						self.xstar(k,mode), xx)[0])**2.)**(1./3)
				#print xx, 'non-propagating'
		return it.operands[1]

	def k_root(self,mode=1,p=None):
		if p is not None:
			self.set_params(p=p)
		cut_on = self.cut_on(mode)
		krange = [2./self.w(0) * cut_on + 1e-3, cut_on - 1e-3]
		try:
			k = spo.brentq(lambda k: self.g(0.,k,mode) - self.eps**(2./3) * self.aiz,\
				krange[0], krange[1])
		except ValueError:
			return sp.nan
		return k

	def __to_grid(self,x,y):
		if isinstance(x, sp.ndarray) and isinstance(y,sp.ndarray):
			return sp.meshgrid(x,y)
		elif isinstance(x, list) and isinstance(y, list):
			return sp.mgrid[x[0]:x[1]:x[2],y[0]:y[1]:y[2]]
		else:
			print "Invalid ranges specified."
			return None

	def __to_real(self,a):
		return sp.imag(a) + sp.real(a)

	def phi(self, x, z, k, mode=1):
		
		phi0 = sp.cos if self.transsymmetry == self.SYMMETRIC else sp.sin
		psi0 = sp.sin if self.transsymmetry == self.ANTISYMMETRIC else sp.cos
		tau = self.tau_n(x, k, mode)

		phi = phi0(tau * z) * ( spc.airy(self.eps**(-2./3) * self.g(x,k,mode))[0])
		return phi

	def st_coeff(self,x,k,coeff='s',mode=1):
		w = self.w(x)
		tpsi = self.tau_n(x,k,mode)
		tphi = sp.sqrt(tpsi**2. + (self.gamma**2. - 1.) * w**2. / 4. *  k**2.)
		if self.transsymmetry == self.SYMMETRIC:
			phi1 = sp.cos(tphi)
			phid1 = - tphi * sp.sin(tphi)
			psi1 = sp.sin(tpsi)
			psid1 = tpsi * sp.cos(tpsi)
			Iphi = 1 + sp.sin(2. * tphi) / (2. * tphi)
			Ipsi = 1 - sp.sin(2. * tpsi) / (2. * tpsi)
		else:
			phi1 = sp.sin(tphi)
			phid1 = tphi * sp.cos(tphi)
			psi1 = sp.cos(tpsi)
			psid1 = - tpsi * sp.sin(tpsi)
			Iphi = 1 - sp.sin(2. * tphi) / (2. * tphi)
			Ipsi = 1 + sp.sin(2. * tpsi) / (2. * tpsi)

		g = self.g(x,k,mode)
		gd = sp.sqrt(abs(4. / w**2. * tpsi**2. - k**2.))/sp.sqrt(abs(g))
		nu = 2 * tpsi**2. - w**2. / 4. * k**2.

		if coeff == 's':
			return 1. / sp.sqrt(\
					( 2. * k**2. / (g * gd**2.) - 4.) \
					* ( w * g * gd / nu ) * phi1 * phid1 \
					- ( nu**2. * phi1**2. ) / (w * gd * psid1**2.) * Ipsi \
					+ ( w * g * gd ) * Iphi\
					)
		else:
			return 1. / sp.sqrt(\
					( 2. * k**2. / (g * gd**2.) - 4.) \
					* ( w * gd / nu ) * phi1 * phid1 \
					- ( nu**2. * phi1**2. ) / (w * g * gd * psid1**2.) * Ipsi \
					+ ( w * gd ) * Iphi\
					)
