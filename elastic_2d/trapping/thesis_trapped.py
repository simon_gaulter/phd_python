import scipy as sp
import scipy.optimize as spo
import scipy.special as spc
import scipy.integrate as spi

class Elastic_trapped:
	"""Methods to calculate trapped mode eigenvalues of the
	slowly-varying elastic plate with a stress free surface.
	"""

	SYMMETRIC = "symmetric"
	ANTISYMMETRIC = "antisymmetric"

	def __init__(self):
		self.set_params(eps=0.1,gamma=0.534,h1=1.5,p=1,\
				longsymmetry=self.SYMMETRIC,transsymmetry=self.ANTISYMMETRIC)
		self.get_params()

	def get_params(self):
		"""Return the current parameters."""
		print "eps:            " + str(self.eps)
		print "h1:             " + str(self.h1)
		print "gamma:          " + str(self.gamma)
		print "longsymmetry:   " + str(self.longsymmetry)
		print "transsymmetry:  " + str(self.transsymmetry)
		print "p:              " + str(self.p)

	def set_params(self,eps=None,gamma=None,h1=None,p=None,\
			longsymmetry=None,transsymmetry=None):
		"""Optionally change the parameters.

		Keyword arguments:
			eps           -- small parameter, epsilon;
			h1            -- bulge height, h1;
			gamma         -- material parameter, c_{psi} / c_{phi}
			p             -- longitudinal mode number;
			transsymmetry -- transverse symmetry, around zeta = 0
			                 ANTISYMMETRIC (flexural) phi antisymmetric / psi symmetric, or 
			                 SYMMETRIC (longitudinal) phi symmetric / psi antisymmetric;
			longsymmetry  -- longitudinal symmetry, around xi = 0"""
		if gamma is not None:
			self.gamma = gamma
		if transsymmetry is not None:
			self.transsymmetry = transsymmetry
		if longsymmetry is not None:
			self.longsymmetry = longsymmetry
		if p is not None:
			self.p = p
		if h1 is not None:
			self.h1 = h1
		if eps is not None:
			self.eps = eps

		# Get the pth root of either Ai (antisymmetric) or Ai' (symmetric)
		if self.longsymmetry == self.SYMMETRIC:
			self.aiz = spc.ai_zeros(self.p)[1][-1]
		else:
			self.aiz = spc.ai_zeros(self.p)[0][-1]

	def hm(self,x):
		"""The lower plate boundary"""
		return 1 + (self.h1 - 1.) * sp.exp(- x**2.)

	def hp(self,x):
		"""The upper plate boundary"""
		return 1 + (self.h1 - 1.) * sp.exp(- x**2.)

	def w(self,x):
		"""The plate width, w = h_- + h_+"""
		return self.hm(x) + self.hp(x)

	def disp_rel_kt(self,x,k,tau):
		"""Calculate the dispersion relation value for
		a given xi, kappa_{psi} and tau_{psi}"""
		w = self.w(x)
		gamma = self.gamma
		tpsi = tau
		tphi = sp.sqrt(tau**2. + (w / 2.)**2. * (gamma**2. - 1.) * k**2.)
		
		if self.transsymmetry == self.ANTISYMMETRIC:
			phi0 = sp.sin
			psi0 = sp.cos
		else:
			phi0 = sp.cos
			psi0 = sp.sin
		return (2. * tpsi**2. - (w / 2.)**2. * k**2.)**2. * phi0(tphi) * psi0(tpsi) - \
				(4. * tpsi**2. - w**2. * k**2.) * tphi * tpsi * phi0(tpsi) * psi0(tphi)

	def bisection(self, f, xmin, xmax, grain=500):
		"""The dispersion relation can be highly oscillatory.
		This is a slow but sure bisection method to find all roots."""
		xlin = sp.linspace(xmin, xmax, grain)
		roots = []
		for i in range(0,len(xlin)-1):
			try:
				x,r = spo.bisect(f, xlin[i], xlin[i+1], full_output=True)
				if r.converged:
					roots.append(x)
			except ValueError:
				pass
		r = sp.asarray(roots)

		# eliminate spurious zeros
		TZERO = 1.0e-6 # tolerance for 'zero'
		r = r[abs(f(r)) < TZERO]
		r = r[r > 0]
		if len(r) == 0:
			return None
		return r

	def tau(self,x,k,mode=None):
		"""Return a value of tau_{psi} for a given xi, kappa_{psi}
		and for a selected mode."""
		dr = lambda tau: self.__to_real(self.disp_rel_kt(x,k,tau))
		# slow but sure
		if mode is not None:
			r = self.bisection(dr,0,self.cut_on(mode)*3,grain=100)
		else:
			r = self.bisection(dr,0,k**2.,grain=500)

		# sort and optionally choose a mode
		TOL = 1.0e-4 # tolerance for 'equality'
		r.sort()
		if mode is not None:
			try:
				return r[mode-1]
			except IndexError:
				return sp.nan
		else:
			prop = r < self.w(x) * k / 2
			return r[sp.hstack([1, sp.diff(r)]) > TOL], prop
		

	def tau_n(self,x,k,n):
		"""Wrapper to return an array of tau_{psi} values."""
		x = sp.asarray(x)
		k = sp.asarray(k)
		if x.ndim == 0: x = sp.expand_dims(x,0)
		if k.ndim == 0: k = sp.expand_dims(k,0)
		try:
			it = sp.nditer([x,k,None],op_axes=[[0,-1],[-1,0],None])
		except ValueError:
			return self.tau(x,k,mode=n)
		for xx, kk, y in it:
			y[...] = self.tau(xx,kk,mode=n)
		return it.operands[2].T

	def cut_on(self,mode):
		"""Return the cut-on frequencies for a given mode."""
		if self.transsymmetry == self.SYMMETRIC:
			r1 = [ n * sp.pi for n in range(1, mode + 2)]
			r2 = [ (2 * n - 1) * sp.pi / (2 * self.gamma) for n in range(1, mode + 2)]
		else:
			r1 = [ (2 * n - 1) * sp.pi / 2. for n in range(1, mode + 2)]
			r2 = [ n * sp.pi / (self.gamma) for n in range(1, mode + 2)]
		return sp.sort(sp.hstack((r1,r2)))[mode-1]

	def xstar(self,k,mode,**kwargs):
		"""Return the turning point, xi*, for a given k and mode number."""
		root = self.cut_on(mode)
		return abs(spo.root(lambda x: 2. * root / k - self.w(x),\
				0,method='broyden1',**kwargs)['x'])

	def g(self,x,k,mode):
		"""Return the phase-like function, g(xi,kappa_{psi})."""
		it = sp.nditer([x,None])
		for xx,y in it:
			if sp.isnan(self.tau_n(xx,k,mode)):
				y[...] = sp.nan
			if 2. / self.w(xx) * self.tau_n(xx,k,mode) < k:
				# This is cut-on
				y[...] = -((3./2 * spi.quad(lambda barx: sp.real(\
						sp.sqrt(k**2 - 4. / self.w(barx)**2. * self.tau_n(barx,k,mode)**2)\
						), xx, self.xstar(k,mode))[0])**2.)**(1./3)
			else:
				# This is cut-off
				y[...] = ((3./2 * spi.quad(lambda barx: sp.real(\
						sp.sqrt(4./ self.w(barx)**2. * self.tau_n(barx,k,mode)**2 - k**2)\
						), self.xstar(k,mode), xx)[0])**2.)**(1./3)
		return it.operands[1]

	def k(self,mode=1,p=None):
		"""Return a single trapped mode wavenumber for the chosen parameters"""
		if p is not None:
			self.set_params(p=p)
		cut_on = self.cut_on(mode)
		krange = [2./self.w(0) * cut_on, cut_on]
		return spo.brentq(lambda k: self.g(0.,k,mode) - self.eps**(2./3) * self.aiz,\
				krange[0], krange[1])

	def __to_real(self,a):
		return sp.imag(a) + sp.real(a)
