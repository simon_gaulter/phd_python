# -*- coding: utf-8 -*-
"""
Created Tuesday 7th August 2012

@author: Simon Gaulter
"""

import numpy as np
import matplotlib.pyplot as plt
from numpy.lib.scimath import sqrt as csqrt
from scipy.optimize import brentq

class Elastic_LO_solution:
    """ Calculate and plot the leading order elastic waveguide solutions. """

    def __init__(self):
        self.set_params(xi=[0,10,.1], eta=[-1,1,.1], omega=2, cT=1, gamma=.547, h0 = 1., h1 = 1.5)

    def set_params(self, xi=None, eta=None, omega=None, cT=None, gamma=None, h0=None, h1=None):
        #if xi is not None and eta is not None:
        #   self.xi, self.eta = np.mgrid[xi[0]:xi[1]+xi[2]:xi[2],eta[0]:eta[1]+eta[2]:eta[2]]
        if xi is not None:
            xxi = np.arange(xi[0], xi[1]+xi[2],xi[2])
            self.xi = xxi.reshape(1, xxi.size)
        if eta is not None:
            eeta = np.arange(eta[0], eta[1]+eta[2],eta[2])
            self.eta = eeta.reshape(eeta.size,1)
        if omega is not None:
            self.omega = omega
        if gamma is not None:
            self.gamma = gamma
        if cT is not None:
            self.cT = cT
        if h0 is not None:
            self.h0 = h0
        if h1 is not None:
            self.h1 = h1

    def h_plus(self, xi):
        return self.h0 + (self.h1 - self.h0) / np.cosh(xi)

    def h_minus(self, xi):
        return self.h0 + (self.h1 - self.h0) / np.cosh(xi)

    def w(self, xi):
        return self.h_plus(xi) + self.h_minus(xi)

    def f_dash(self, xi):
        """ Numerically solve the dispersion relation to get f'. """
        # TODO - set self.omega
        omega = self.omega
        gamma = self.gamma
        cT = self.cT
        w = self.w

        f_dash = np.array([])
        for x in np.nditer(xi):
            kS = lambda f_dash: ( w(x)/2. )**2 * f_dash**2 \
                    * np.sinh( w(x)/2. * csqrt(f_dash**2 - omega**2 / cT**2) ) \
                    * np.cosh( w(x)/2. * csqrt(f_dash**2 - gamma**2 * omega**2 / cT**2) ) \
                    - w(x)/2. * csqrt(f_dash**2 - gamma**2 * omega**2 / cT**2) \
                    * w(x)/2. * csqrt(f_dash**2 - omega**2 / cT**2) \
                    * np.sinh( w(x)/2. * csqrt(f_dash**2 - gamma**2 * omega**2 / cT**2) ) \
                    * np.cosh( w(x)/2. * csqrt(f_dash**2 - omega**2 / cT**2) )

            # TODO - we've got an arbitrary interval for f_dash here
            # and are only choosing one f_dash.
            f_dash = np.append(f_dash, brentq(kS, 0, 10))
        return f_dash

    def nuL(self, xi):
        omega = self.omega
        gamma = self.gamma
        cT = self.cT
        w = self.w
        f_dash = self.f_dash

        return w(xi)/2. * csqrt( f_dash(xi)**2 - gamma**2 * omega**2 / cT**2 )

    def nuT(self, xi):
        omega = self.omega
        cT = self.cT
        w = self.w
        f_dash = self.f_dash

        return w(xi)/2. * csqrt( f_dash(xi)**2 - omega**2 / cT**2 )

    def alpha(self, xi):
        nL = self.nuL(xi)
        nT = self.nuT(xi)

        w = self.w
        f_dash = self.f_dash

        G = np.sinh(2.*nL) * ( 1 / (2.*nL) + nL / (2.*nT**2) - nL / (nT * np.sinh(2.*nT))) \
                - 2. * (np.cosh(nL))**2 * np.tanh(nT) / nT + 1

        return csqrt(1j / ( w(xi) * f_dash(xi) * G))

    def lo_solutions(self):
        xi = self.xi
        eta = self.eta

        w = self.w
        alpha = self.alpha
        nuL = self.nuL
        nuT = self.nuT
        f_dash = self.f_dash

        self.a = alpha(xi) * np.cosh(nuL(xi) * eta)
        
        self.b = 1j * w(xi) * f_dash(xi) * np.cosh(nuL(xi)) \
                / (2 * nuT(xi) * np.cosh(nuT(xi))) * alpha(xi) * np.sinh(nuT(xi) * eta)
