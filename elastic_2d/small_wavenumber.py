import numpy as np
from matplotlib.pyplot import *

class Small_wavenumber:

    def __init__(self):
        self.set_params(cT=1, gamma=0.547, w = 2, n = [1,20])

    def set_params(self, cT=None, gamma=None, w=None, n=None):
        if cT is not None:
            self.cT = cT
        if gamma is not None:
            self.gamma = gamma
        if w is not None:
            self.w = w
        if n is not None:
            self.n = np.arange(n[0], n[1]+1)

    def symm_omega_0(self):
        cT = self.cT
        gamma = self.gamma
        w = self.w
        n = self.n

        c_roots = 2. * cT * np.pi * (n - 1/2.) / w
        s_roots = 2. * cT * np.pi * n / (gamma * w)

        self.symm_omega_0_roots = np.hstack([c_roots, s_roots])
        return self.symm_omega_0_roots

    def symm_omega_2(self):
        cT = self.cT
        gamma = self.gamma
        w = self.w
        n = self.n

        c_roots = cT * w / (2. * np.pi * (n - 1/2.))\
                * (w / (2. * np.pi * (n - 1/2.) * gamma**2)\
                / np.tan(gamma * np.pi * (n - 1/2.)) + 1/2.)

        s_roots = cT * gamma * w / (2. * n * gamma * np.pi)\
                * (1/2. - w / (2. * n * np.pi) \
                * np.tan(n * np.pi / gamma))

        self.symm_omega_2_roots = np.hstack([c_roots, s_roots])
        return self.symm_omega_2_roots

    def antisymm_omega_0(self):
        cT = self.cT
        gamma = self.gamma
        w = self.w
        n = self.n

        c_roots = 2. * cT * np.pi * (n - 1/2.) / (gamma * w)
        s_roots = 2. * cT * np.pi * n / w
        
        self.antisymm_omega_0_roots = np.hstack([c_roots, s_roots])
        return self.antisymm_omega_0_roots

    def antisymm_omega_2(self):
        cT = self.cT
        gamma = self.gamma
        w = self.w
        n = self.n

        c_roots = cT * w / (2. * gamma * np.pi * (n - 1/2.))\
                * (1/2. + w / (2. * np.pi * (n - 1/2.))\
                / (np.tan(np.pi * (n - 1/2.) / gamma)))

        s_roots = cT * w / (2. * n * np.pi) \
                * (1/2. - w * np.tan(gamma * n * np.pi) \
                / (2. * gamma**2 * n * np.pi))
        self.antisymm_omega_2_roots = np.hstack([c_roots, s_roots])
        return self.antisymm_omega_2_roots

    def points_omega(self, plot=True):
        symm_o_0 = self.symm_omega_0()
        symm_o_2 = self.symm_omega_2()
        symm_omega = np.vstack([symm_o_0, symm_o_2]).T
        symm_omega = symm_omega[symm_omega[:,0].argsort()]

        antisymm_o_0 = self.antisymm_omega_0()
        antisymm_o_2 = self.antisymm_omega_2()
        antisymm_omega = np.vstack([antisymm_o_0, antisymm_o_2]).T
        antisymm_omega = antisymm_omega[antisymm_omega[:,0].argsort()]

        self.antisymm_omega = antisymm_omega
        self.symm_omega = symm_omega
        if plot:
            ion()

            plot(symm_omega[:,0],symm_omega[:,1], 'bo', antisymm_omega[:,0], antisymm_omega[:,1], 'ro')
            grid(which='both')
            xlabel('$\omega_0$')
            ylabel('$\omega_2$')
